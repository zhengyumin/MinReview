//
//  LandscapeCmd.h
//  MinReview
//
//  Created by zhengyumin on 17/5/25.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseCommand.h"

// 获取景点列表
@interface GetViewspotCmd : BaseCommand
@property (nonatomic, strong) NSString *act;
@property (nonatomic, assign) int page;
@end
