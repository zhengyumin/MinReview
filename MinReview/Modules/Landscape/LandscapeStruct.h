//
//  LandscapeStruct.h
//  MinReview
//
//  Created by zhengyumin on 17/5/25.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseDB.h"

// 景点
@interface Viewspot: BaseDB
@property(nonatomic, assign) int goods_id; //景点id
@property(nonatomic, strong) NSString *goods_name;//景点名称
@property(nonatomic, strong) NSString *address;//景点地址描述
@property(nonatomic, assign) int click_count;//景点点击量
@property(nonatomic, assign) double latitude;//景点纬度
@property(nonatomic, assign) double longitude;//景点经度
@end

