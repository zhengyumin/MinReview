//
//  LandscapeCmd.m
//  MinReview
//
//  Created by zhengyumin on 17/5/25.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "LandscapeCmd.h"

//获取景点列表
@implementation GetViewspotCmd

- (id) init {
    if (self = [super init]) {
        self.addr = @"viewspot.php";
    }
    return self;
}
@end
