//
//  LoginStruct.h
//  MinReview
//
//  Created by zhengyumin on 17/5/25.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseDB.h"

//登陆返回的uid
@interface LoginReturn : BaseDB
@property(nonatomic, copy) NSString *user_name;
@property(nonatomic, assign) int user_id;
@property(nonatomic, assign) int sex;
@end
