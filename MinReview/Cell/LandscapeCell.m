//
//  LandscapeCell.m
//  MinReview
//
//  Created by zhengyumin on 17/5/25.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "LandscapeCell.h"

@interface LandscapeCell()

@property (weak, nonatomic)     UILabel *titleLabel;
@property (weak, nonatomic)     UILabel *subTitleLabel;
@property (weak, nonatomic)     UILabel *distanceLabel;/**<距离>*/

@end

@implementation LandscapeCell

#pragma mark - life cycle
+ (instancetype)cellWithTableView:(UITableView *)tableView{
    static NSString *ID = @"LandscapeCell";
    LandscapeCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (cell == nil) {
        cell = [[LandscapeCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ID];
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self initUI];
    }
    return self;
}

#pragma mark - InitUI
- (void)initUI{
    UILabel *label = [[UILabel alloc] init];
    _distanceLabel = label;
    _distanceLabel.textColor = MRTextGray;
    _distanceLabel.font = [UIFont systemFontOfSize:13];
    _distanceLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:_distanceLabel];
}

- (void)layoutSubviews{
    [super layoutSubviews];
    [self.textLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.contentView.mas_left).offset(20);
        make.top.mas_equalTo(self.contentView.mas_top).offset(25);
        make.height.mas_equalTo(21);
        make.right.mas_equalTo(self.contentView.mas_right).offset(-80);
    }];
    [_distanceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(self.contentView.mas_right).offset(-15);
        make.top.mas_equalTo(self.contentView.mas_top).offset(15);
        make.height.mas_equalTo(15);
        make.width.mas_equalTo(80);
    }];
}

#pragma mark - getters and setters
- (void)setFrame:(CGRect)frame{
    CGFloat padding = 8;
    frame.size.width = SCREEN_WIDTH - padding * 2;
    frame.origin.x = padding;
    [super setFrame:frame];
}


/**
 设置景点数据
 @param viewspot 景点对象
 */
- (void)setViewspot:(Viewspot *)viewspot{
    self.textLabel.text = viewspot.goods_name;
    self.detailTextLabel.text = viewspot.address;
}

@end
