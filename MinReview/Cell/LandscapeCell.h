//
//  LandscapeCell.h
//  MinReview
//
//  Created by zhengyumin on 17/5/25.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MinHelper.h"

@interface LandscapeCell : UITableViewCell

@property (strong, nonatomic) Viewspot *viewspot;
@property (strong, nonatomic) UIViewController *viewcontroller;
+ (instancetype)cellWithTableView:(UITableView *)tableView;

@end
