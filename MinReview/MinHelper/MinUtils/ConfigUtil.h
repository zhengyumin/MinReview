//
//  ConfigUtil.h
//  MinReview
//
//  Created by zhengyumin on 17/5/24.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfigUtil : NSObject

+ (int) intWithKey:(NSString*) key;

+ (void) saveInt:(int) val withKey:(NSString*) key;

+ (BOOL) boolWithKey:(NSString*) key;

+ (BOOL) boolWithKey:(NSString*) key default:(BOOL) def;

+ (void) saveBool:(BOOL) val withKey:(NSString*) key;

+ (double)doubleWithKey:(NSString *)key;

+ (void)saveDouble:(double)val withKey:(NSString *)key;

+ (NSString*)stringWithKey:(NSString *)key;

+ (void)saveString:(NSString*)val withKey:(NSString *)key;

+ (BOOL)isalreadyHasObject:(NSString *)key;

@end
