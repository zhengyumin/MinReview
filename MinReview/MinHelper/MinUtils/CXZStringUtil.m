//
//  CXZStringUtil.m
//  ImHere
//
//  Created by 卢明渊 on 15/3/19.
//  Copyright (c) 2015年 我在这. All rights reserved.
//

//#import "CXZStringUtil.h"
//#import "MarkupParser.h"
//#import "CustomMethod.h"
//#import "NSAttributedString+Attributes.h"
//#import "CXZ.h"
//@implementation CXZStringUtil
//
//+ (CGFloat)labelHeightWithContent:(NSString *)content withWidth:(CGFloat)width {
//    NSString* path = [[NSBundle mainBundle] pathForResource:@"faceMap_ch" ofType:@"plist"];
//    NSString *text = [CustomMethod transformString:content emojiDic:[NSDictionary dictionaryWithContentsOfFile:path]];
//    MarkupParser *wk_markupParser = [[MarkupParser alloc] init];
//    NSMutableAttributedString* attString = [wk_markupParser attrStringFromMarkup: text];
//    [attString setFont:[UIFont systemFontOfSize:14.0f]];
//    CGSize size = [attString sizeConstrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
//    return size.height;
//}
//+ (CGSize)labeSizeWithContent:(NSString *)content withWidth:(CGFloat)width {
//    NSString* path = [[NSBundle mainBundle] pathForResource:@"faceMap_ch" ofType:@"plist"];
//    NSString *text = [CustomMethod transformString:content emojiDic:[NSDictionary dictionaryWithContentsOfFile:path]];
//    MarkupParser *wk_markupParser = [[MarkupParser alloc] init];
//    NSMutableAttributedString* attString = [wk_markupParser attrStringFromMarkup: text];
//    [attString setFont:[UIFont systemFontOfSize:14.0f]];
//    CGSize size = [attString sizeConstrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
//    return size;
//}
//
//+ (CGSize)setLabel:(OHAttributedLabel *)label withContent:(NSString *)content withWidth:(CGFloat)width {
//    NSString * path=[[NSBundle mainBundle] pathForResource:@"faceMap_ch" ofType:@"plist"];
//    NSString *text = [CustomMethod transformString:content emojiDic:[NSDictionary dictionaryWithContentsOfFile:path]];
//    MarkupParser *wk_markupParser = [[MarkupParser alloc] init];
//    NSMutableAttributedString* attString = [wk_markupParser attrStringFromMarkup: text];
//    [attString setFont:[UIFont systemFontOfSize:14]];
//    [label setLineBreakMode:NSLineBreakByCharWrapping];
//    [label setAttString:attString withImages:wk_markupParser.images];
//    return [CXZStringUtil labeSizeWithContent:content withWidth:width];
//}
//
//+ (CGSize)setLabel:(OHAttributedLabel *)label withForwardContent:(NSString *)content withSize:(CGSize)size {
//    NSError* error;
//    NSArray* data = [NSJSONSerialization JSONObjectWithData:[content dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:&error];
//    NSMutableString *forward_content = [[NSMutableString alloc]init];
//    [label removeAllCustomLinks];
//    NSMutableDictionary* linkMap = [NSMutableDictionary dictionary];
//    for(NSDictionary* item in data) {
//        NSString* nick = [item objectForKey:@"nickname"];
//        NSString* word = [item objectForKey:@"word"];
//        if(item == [data firstObject]){
//            [forward_content appendString:[NSString stringWithFormat:@"%@", word]];
//        }else{
//            [forward_content appendString:[NSString stringWithFormat:@"//@%@:%@", nick, word]];
//            NSString* key = [NSString stringWithFormat:@"@%@:", nick];
//            [linkMap setObject:[NSURL URLWithString:[item objectForKey:@"uid"]] forKey:key];
//        }
//    }
//    
//    NSString * path=[[NSBundle mainBundle] pathForResource:@"faceMap_ch" ofType:@"plist"];
//    NSString *text = [CustomMethod transformString:forward_content emojiDic:[NSDictionary dictionaryWithContentsOfFile:path]];
//    MarkupParser *wk_markupParser = [[MarkupParser alloc] init];
//    NSMutableAttributedString* attString = [wk_markupParser attrStringFromMarkup: text];
//    [attString setFont:[UIFont systemFontOfSize:14]];
//    [label setLineBreakMode:NSLineBreakByCharWrapping];
//    [label setAttString:attString withImages:wk_markupParser.images];
//    
//    NSString* resultString = attString.string;
//    NSRange currentRange = {0, resultString.length};
//    for (NSString* key in linkMap) {
//        currentRange = [resultString rangeOfString:key options:0 range:currentRange];
//        [label addCustomLink:[linkMap objectForKey:key] inRange:currentRange];
//        currentRange.location = currentRange.location+currentRange.length;
//        currentRange.length = resultString.length-currentRange.location;
//    }
//    CGFloat height = [CXZStringUtil labelHeightWithContent:forward_content withWidth:size.width];
//    if (size.height == 0) {
//        return CGSizeMake(size.width, height);
//    } else {
//        return CGSizeMake(size.width, MIN(size.height, height));
//    }
//}
//
//+(CGSize)setLabel:(OHAttributedLabel *)label withForwardContent:(NSString *)content withWidth:(CGFloat)width withLenth:(int)nameLength{
//    NSString * path=[[NSBundle mainBundle] pathForResource:@"faceMap_ch" ofType:@"plist"];
//    NSString *text = [CustomMethod transformString:content emojiDic:[NSDictionary dictionaryWithContentsOfFile:path]];
//    MarkupParser *wk_markupParser = [[MarkupParser alloc] init];
//    NSMutableAttributedString* attString = [wk_markupParser attrStringFromMarkup: text];
//    [attString setFont:[UIFont systemFontOfSize:14]];
//    [label setLineBreakMode:NSLineBreakByCharWrapping];
//    [label setAttString:attString withImages:wk_markupParser.images];
//    [attString addAttribute:NSForegroundColorAttributeName value:HexRGB(0x0070BD) range:NSMakeRange(2, nameLength)];
//    return [CXZStringUtil labeSizeWithContent:content withWidth:width];
//}
//
//+ (NSString *)getRealForwardContent:(NSString *)content {
//    NSError* error;
//    NSArray* data = [NSJSONSerialization JSONObjectWithData:[content dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:&error];
//    NSMutableString *forward_content = [[NSMutableString alloc]init];
//    for(NSDictionary* item in data) {
//        NSString* nick = [item objectForKey:@"nickname"];
//        NSString* word = [item objectForKey:@"word"];
//        if(item == [data firstObject]){
//            [forward_content appendString:[NSString stringWithFormat:@"%@",word]];
//        }else{
//            [forward_content appendString:[NSString stringWithFormat:@"//@%@:%@", nick, word]];
//        }
//    }
//    return forward_content;
//}

//@end
