//
//  CXZStringUtil.h
//  ImHere
//
//  Created by 卢明渊 on 15/3/19.
//  Copyright (c) 2015年 我在这. All rights reserved.
//

//#import <UIKit/UIKit.h>
//#import "OHAttributedLabel.h"
////#import "MomentStruct.h"
//
//@interface CXZStringUtil : NSObject
//
//// 获取带表情的实际文本高度
//+ (CGFloat) labelHeightWithContent:(NSString*)content withWidth:(CGFloat) width;
//
//// 设置带表情的文本
//+ (CGSize) setLabel:(OHAttributedLabel*) label withContent:(NSString*) content withWidth:(CGFloat) width;
//
//// 设置带表情带链接的转发文本
//+ (CGSize) setLabel:(OHAttributedLabel*) label withForwardContent:(NSString*) content withSize:(CGSize) size;
//
//// 设置回复别人的回复且带表情的文本
//+ (CGSize) setLabel:(OHAttributedLabel*) label withForwardContent:(NSString*) content withWidth:(CGFloat) width withLenth:(int)nameLength;
//
//// 获取实际的转发文本
//+ (NSString*) getRealForwardContent:(NSString*)content;
//@end
