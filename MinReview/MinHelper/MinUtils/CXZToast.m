//
//  CXZToast.m
//  stamp
//
//  Created by 卢明渊 on 15/6/19.
//  Copyright (c) 2015年 ZhaoYuze. All rights reserved.
//

#import "CXZToast.h"
#import "CRToast.h"
#import <UIKit/UIKit.h>
#import "MinHelper.h"

@implementation CXZToast

+ (NSMutableDictionary*) baseOptions {
    NSMutableDictionary *options = [@{kCRToastNotificationTypeKey               : @(CRToastTypeNavigationBar),
                                      kCRToastNotificationPresentationTypeKey   : @(CRToastPresentationTypeCover),
                                      kCRToastUnderStatusBarKey                 : @(NO),
                                      kCRToastTimeIntervalKey                   : @(1.0f),
                                      kCRToastAnimationInTypeKey                : @(CRToastAnimationTypeSpring),
                                      kCRToastAnimationOutTypeKey               : @(CRToastAnimationTypeLinear),
                                      kCRToastAnimationInDirectionKey           : @(0),
                                      kCRToastAnimationOutDirectionKey          : @(0)} mutableCopy];
    
    options[kCRToastTextAlignmentKey] = @(NSTextAlignmentLeft);
    options[kCRToastBackgroundColorKey] = HexRGBAlpha(0x0, 0.8);
    options[kCRToastFontKey] = [UIFont systemFontOfSize:15];
    options[kCRToastInteractionRespondersKey] = @[[CRToastInteractionResponder interactionResponderWithInteractionType:CRToastInteractionTypeTap
                                                                                                  automaticallyDismiss:YES
                                                                                                                 block:^(CRToastInteractionType interactionType){
                                                                                                                     NSLog(@"Dismissed with %@ interaction", NSStringFromCRToastInteractionType(interactionType));
                                                                                                                 }]];
    return options;
}

+ (void) showError:(NSString*) toast {
    NSMutableDictionary* options = [CXZToast baseOptions];
    options[kCRToastImageKey] = [UIImage imageNamed:@"cxz_alert_icon"];
    options[kCRToastTextKey] = toast;
    [CRToastManager showNotificationWithOptions:options completionBlock:nil];
}

+ (void) showSuccess:(NSString*) toast {
    NSMutableDictionary* options = [CXZToast baseOptions];
    options[kCRToastImageKey] = [UIImage imageNamed:@"cxz_white_checkmark"];
    options[kCRToastTextKey] = toast;
    [CRToastManager showNotificationWithOptions:options completionBlock:nil];
}

+ (void) showToast:(NSString*) title SubTitle:(NSString*) subTitle {
    NSMutableDictionary* options = [CXZToast baseOptions];
    options[kCRToastTextKey] = title;
    options[kCRToastSubtitleTextKey] = subTitle;
    options[kCRToastSubtitleTextAlignmentKey] = @(NSTextAlignmentCenter);
    options[kCRToastTextAlignmentKey] = @(NSTextAlignmentCenter);
    options[kCRToastFontKey] = [UIFont boldSystemFontOfSize:16];
    options[kCRToastSubtitleFontKey] = [UIFont systemFontOfSize:12];
    [CRToastManager showNotificationWithOptions:options completionBlock:nil];
}

@end
