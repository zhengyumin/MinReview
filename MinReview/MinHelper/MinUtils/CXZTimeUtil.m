//
//  TimeUtil.m
//  SkylineLibrary-ios
//
//  Created by waikeungshen on 15/5/28.
//  Copyright (c) 2015年 waikeungshen. All rights reserved.
//

#import "CXZTimeUtil.h"

@implementation CXZTimeUtil

+ (id) getInstance {
    static CXZTimeUtil *singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[CXZTimeUtil alloc] init];
    });
    return singleton;
}

- (NSInteger)currentSecond {
    NSDate *now = [NSDate date];
    return [now timeIntervalSince1970];
}

- (long long)currentMsec {
    NSDate *now = [NSDate date];
    //DLog(@"time : %.f", [now timeIntervalSince1970] * 1000);
    return [now timeIntervalSince1970] * 1000;
}

- (NSDateComponents *)currentDateCompoent {
    NSDate *now = [NSDate date];
    return [self dateCompoentFromDate:now];
}

- (NSDateComponents *)dateCompoentFromDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSHourCalendarUnit | NSMinuteCalendarUnit | NSSecondCalendarUnit;
    return [calendar components:unitFlags fromDate:date];
}

- (NSInteger)maxDayForYear:(NSInteger *)year month:(NSInteger)month {
    NSString *date = [NSString stringWithFormat:@"%04ld-%02ld-01", year, month];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *today = [formatter dateFromString:date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSRange days = [calendar rangeOfUnit:NSDayCalendarUnit
                           inUnit:NSMonthCalendarUnit
                          forDate:today];
    
    return days.length;
}

- (NSDate *)dateFromYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day hour:(NSInteger)hour minute:(NSInteger)minute second:(NSInteger)second {
    NSDateComponents* components = [[NSDateComponents alloc] init];
    [components setYear:year];
    [components setMonth:month];
    [components setDay:day];
    [components setHour:hour];
    [components setMinute:minute];
    [components setSecond:second];
    NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    return [calendar dateFromComponents:components];
}

- (NSString *)dateStringFromSecond:(NSInteger)second {
    NSDate * date = [NSDate dateWithTimeIntervalSince1970:second];
    NSDateComponents *dateComponent = [self dateCompoentFromDate:date];
    return [NSString stringWithFormat:@"%04ld-%02ld-%02ld %02ld:%02ld:%02ld", dateComponent.year, dateComponent.month, dateComponent.day, dateComponent.hour, dateComponent.minute, dateComponent.second];
}

- (NSDate *)dateAfterFromDate:(NSDate *)date withYear:(NSInteger)year month:(NSInteger)month day:(NSInteger)day {
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setYear:year];
    [comps setMonth:month];
    [comps setDay:day];
    NSCalendar *calender = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDate *mDate = [calender dateByAddingComponents:comps toDate:date options:0];
    return mDate;
}

- (NSInteger)differenceFromDate:(NSDate *)date1 toDate:(NSDate *)date2 {
    //得到相差秒数
    NSTimeInterval time = [date2 timeIntervalSinceDate:date1];
    int days = ((int)time)/(3600*24);
    return days;
}

- (NSString *)formatDifferenceToNowFromSecond:(NSInteger)second {
    NSInteger nowSecond = [self currentSecond];
    NSInteger diff = nowSecond - second;
    if (diff < 60) { // 几秒前
        return [NSString stringWithFormat:@"%ld秒前", diff];
    }
    if (diff >= 60 && diff < 3600) { // 几分钟前
        return [NSString stringWithFormat:@"%ld分钟前", diff/60];
    }
    if (diff >= 3600 && diff < 3600*24) { // 几小时前
        return [NSString stringWithFormat:@"%ld小时前", diff/3600];
    }
    // 几天前
    return [NSString stringWithFormat:@"%ld天前", diff/(3600*24)];
}

@end
