//
//  CXZToast.h
//  stamp
//
//  Created by 卢明渊 on 15/6/19.
//  Copyright (c) 2015年 ZhaoYuze. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CXZToast : NSObject

+ (void) showError:(NSString*) toast;

+ (void) showSuccess:(NSString*) toast;

+ (void) showToast:(NSString*) title SubTitle:(NSString*) subTitle;
@end
