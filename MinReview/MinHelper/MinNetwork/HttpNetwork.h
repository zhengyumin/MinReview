//
//  HttpNetwork.h
//  MinReview
//
//  Created by zhengyumin on 17/5/25.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BaseRespond;
@class ASINetworkQueue;
@class BaseCommand;

typedef void (^SuccessBlock)(BaseRespond* respond);
typedef void (^FailedBlock)(BaseRespond* respond, NSString* error);

@interface HttpNetwork : NSObject

@property(nonatomic, copy) void(^checkTokenFailed)();

+ (HttpNetwork*) getInstance;
- (void )requestWithAFN:(BaseCommand*)cmd success:(SuccessBlock)success failed:(FailedBlock)failed;
@end
