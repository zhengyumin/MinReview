//
//  HttpNetwork.m
//  MinReview
//
//  Created by zhengyumin on 17/5/25.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "HttpNetwork.h"
#import "MinHelper.h"

@interface HttpNetwork()

@property(nonatomic, strong) NSMutableDictionary* cmdDic;

@end

@implementation HttpNetwork

+ (HttpNetwork *)getInstance{
    static HttpNetwork *s_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_instance = [HttpNetwork new];
    });
    return s_instance;
}

- (id)init{
    if (self = [super init]) {
        _cmdDic = [[NSMutableDictionary alloc] init];
    }
    return self;
}

- (void )requestWithAFN:(BaseCommand*)cmd success:(SuccessBlock)success failed:(FailedBlock)failed{
    
    // 3秒之内重复请求过滤
    NSString* key = [NSString stringWithFormat:@"%@",cmd.addr];
    int now = [[NSDate date] timeIntervalSince1970];
    if ([_cmdDic objectForKey:key] && (now - [[_cmdDic objectForKey:key] intValue]) < 4) {
        return ;
    }
    NSLog(@"%@", key);
    [_cmdDic setObject:[NSNumber numberWithInt:now] forKey:key];
    
    //配置请求参数
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/json",@"text/html",  @"application/json", nil] ;
    
    NSString *url = [NSString stringWithFormat:@"%@%@", API_HOST, cmd.addr];
    NSDictionary *dic = [cmd mj_keyValues];
    
    [manager GET:url parameters:dic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        [[HttpNetwork getInstance].cmdDic removeObjectForKey:key];
        BaseRespond* respond = [BaseRespond mj_objectWithKeyValues:responseObject];
        
        if(respond.error == 0) {
            if (success) {
                success(respond);
            }
        }else {//token不正确
            if (respond.error == -110 && [HttpNetwork getInstance].checkTokenFailed != nil) {
                [HttpNetwork getInstance].checkTokenFailed();
            } else {
                if (failed) {
                    failed(respond, respond.message);
                }
            }
        }

    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failed) {
            NSString* errorStr = error.localizedFailureReason;
            if(IS_EMPTY(errorStr)) {
                errorStr = @"网络异常";
            }
            failed(nil, errorStr);
        }
    }];

}


@end
