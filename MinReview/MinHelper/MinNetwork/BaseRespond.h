//
//  BaseRespond.h
//  MinReview
//
//  Created by zhengyumin on 17/5/25.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#ifndef MinReview_Respond_h
#define MinReview_Respond_h

#import <Foundation/Foundation.h>

#pragma mark - 消息响应基础类
@interface BaseRespond : NSObject
@property(nonatomic, assign) int error;
@property(nonatomic, copy) NSString* message;
@property(nonatomic, strong) id data;
@end

#endif
