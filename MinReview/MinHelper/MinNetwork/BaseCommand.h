//
//  BaseCommand.h
//  MinReview
//
//  Created by zhengyumin on 17/5/25.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#ifndef MinReview_Command_h
#define MinReview_Command_h

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#pragma mark - 消息基础类
@interface BaseCommand : NSObject
@property(nonatomic, assign) int uid; // 自己的uid
@property(nonatomic, copy) NSString* token; // token 验证
@property(nonatomic, copy) NSString* addr; // 本地使用 拼接url用的
@property(nonatomic, copy) Class respondType;
@end


//登录命令
@interface LoginRequest : BaseCommand
@property(nonatomic, copy) NSString* act;
@property(nonatomic, copy) NSString* tel; // 用户名
@property(nonatomic, copy) NSString* pwd; // 密码
@end

// 第三方登陆
@interface ThirdLogin : BaseCommand
@property(nonatomic, copy) NSString* plat_id;
@property(nonatomic, copy) NSString* plat_name;
@end

// 第三方手机绑定
@interface ThirdMobile : BaseCommand
@property(nonatomic, copy) NSString* tel;
@property(nonatomic, copy) NSString* code; // 密码
@property(nonatomic, copy) NSString* plat_id;
@property(nonatomic, copy) NSString* plat_name;
@end

// 注册
@interface Register : BaseCommand
@property(nonatomic, copy) NSString* account;
@property(nonatomic, copy) NSString* nick;
@property(nonatomic, copy) NSString* password;
@property(nonatomic, assign) int type;
@property(nonatomic, copy) NSString* inviter;
@end

// 检查帐号
@interface CheckAccount : BaseCommand
@property(nonatomic, copy) NSString* account;
@property(nonatomic, assign) int type; // 1-手机，2-qq，3-微信，4-微博
@end

//重置密码
@interface ResetPassword : BaseCommand
@property(nonatomic, copy) NSString* mobile;
@property(nonatomic, copy) NSString* newpass;

@end

//修改密码
@interface EditPassword : BaseCommand
@property(nonatomic, copy) NSString *mobile;
@property(nonatomic, copy) NSString* oldpass;
@property(nonatomic, copy) NSString* newpass;

- (void)editPassword:(NSString*)oldpass NewPass:(NSString*)newpass mobile:(NSString *)mobile;

@end

#endif
