//
//  BaseCommand.m
//  MinReview
//
//  Created by zhengyumin on 17/5/25.
//  Copyright © 2017年 hikvision. All rights reserved.
//


#import "MinHelper.h"

@implementation BaseCommand

- (id) init {
    if (self = [super init]) {
        self.addr = @"";
        self.respondType = [BaseRespond class];
        self.uid = [ConfigUtil intWithKey:UID];
        self.token = [ConfigUtil stringWithKey:TOKEN];
    }
    return self;
}
@end

//登录命令
@implementation LoginRequest

- (id) init {
    if (self = [super init]) {
        self.addr = @"user.php";
        self.respondType = [BaseRespond class];
    }
    return self;
}

@end

@implementation ThirdMobile

- (id) init {
    if (self = [super init]) {
        self.addr = @"/user/third-code";
        self.respondType = [BaseRespond class];
    }
    return self;
}

@end


@implementation ThirdLogin

- (id) init {
    if (self = [super init]) {
        self.addr = @"/user/third";
        self.respondType = [BaseRespond class];
    }
    return self;
}

@end

// 注册
@implementation Register

- (id) init {
    if (self = [super init]) {
        self.addr = @"/User/register";
        self.type = 1;
        self.respondType = [BaseRespond class];
    }
    return self;
}

@end

// 检查帐号
@implementation CheckAccount

- (id) init {
    if (self = [super init]) {
        self.addr = @"/User/check_account";
        self.respondType = [BaseRespond class];
    }
    return self;
}

@end


//重置密码
@implementation ResetPassword

- (id) init {
    if (self = [super init]) {
        self.addr = @"/User/reset_password";
        self.respondType = [BaseRespond class];
    }
    return self;
}

@end

//修改密码
@implementation EditPassword

- (id) init {
    if (self = [super init]) {
        self.addr = @"/User/change_password";
        self.respondType = [BaseRespond class];
    }
    return self;
}

- (void)editPassword:(NSString*)oldpass NewPass:(NSString*)newpass mobile:(NSString *)mobile {
    self.mobile  = mobile;
    self.oldpass = oldpass;
    self.newpass = newpass;
}

@end

