//
//  UIImage+Resize.h
//  MinReview
//
//  Created by zhengyumin on 17/5/22.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Resize)

/**
 *  返回一张自由拉伸的图片
 */
+ (UIImage *)resizedImageWithName:(NSString *)name;

+ (UIImage *)imageWithColor:(UIColor *)color;
/**
 *  Creates and returns UIImage instance with selected color and size
 *
 *  @param color selected color
 *  @param size  selected size
 *
 *  @return UIImage for given parameters
 */
+ (UIImage *)imageWithColor:(UIColor *)color andSize:(CGSize)size;

/**
 *  返回一张自由裁剪的图片
 */
-(UIImage*)getSubImage:(CGRect)rect;

@end
