//
//  NSString+Category.m
//  MinReview
//
//  Created by zhengyumin on 17/5/19.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "NSString+Category.h"
#import "MinHelper.h"

@implementation NSString (Category)

+ (BOOL) isEmpty:(NSString *)str {
    if (str && str.length > 0) {
        return false;
    }
    return true;
}

- (CGSize)sizeWithFont:(UIFont *)font width:(CGFloat)width {
    CGSize size = CGSizeMake(width, CGFLOAT_MAX);
    if(IOS8) {
        size = [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:[NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName] context:nil].size;
    }
 
    return size;
}

// 字符个数统计
- (int)charCounts {
    int strlength = 0;
    char* p = (char*)[self cStringUsingEncoding:NSUnicodeStringEncoding];
    for (int i=0 ; i<[self lengthOfBytesUsingEncoding:NSUnicodeStringEncoding] ;i++) {
        if (*p) {
            p++;
            strlength++;
        }
        else {
            p++;
        }
    }
    return strlength;
}

- (NSString *)ToChineseAmount{
    if(self.length == 0)
        return @"";
    NSRange range = [self rangeOfString:@"."];
    NSInteger begin = [self integerValue];
    NSInteger end = 0;
    if(range.length > 0)
    {
        begin = [[self substringToIndex:range.location] integerValue];
        NSString *endstr = [self substringFromIndex:range.location];
        float endF = [endstr floatValue];
        end = endF * 100;
    }
    NSMutableString *result = [[NSMutableString alloc] init];
    NSArray *ChinaUnits = @[@"仟", @"佰", @"拾", @""];
    NSArray *ChinaUnitss = @[@"亿", @"万", @"圆"];
    NSArray *ChinaNumbers = @[@"零", @"壹", @"贰", @"叁", @"肆", @"伍", @"陆", @"柒", @"捌", @"玖"];
    // 圆
    NSInteger base = 100000000;
    NSInteger temp = begin / base;
    begin %= base;
    for(int j = 0; j < 3; j++)
    {
        if(temp > 0)
        {
            NSInteger d = 1000;
            for(int i = 0; i < 4; i++)
            {
                NSInteger index = temp / d;
                temp %= d;
                d /= 10;
                if(index == 0 && result.length > 0)
                {
                    if(d > 0 && temp / d > 0)
                        [result appendFormat:@"%@", ChinaNumbers[index]];
                }
                else if(index > 0 && index < 10)
                    [result appendFormat:@"%@%@", ChinaNumbers[index], ChinaUnits[i]];
            }
            if(result.length > 0)
                [result appendString:ChinaUnitss[j]];
        }
        base /= 10000;
        if(base > 0)
        {
            temp = begin / base;
            begin %= base;
        }
    }
    range = [result rangeOfString:@"圆"];
    if(range.length == 0)
        [result appendString:@"圆"];
    // 角、分
    if(end > 0)
    {
        temp = end / 10;
        if(temp > 0)
            [result appendFormat:@"%@角", ChinaNumbers[temp]];
        else
            [result appendString:@"零"];
        temp = end % 10;
        if(temp > 0)
            [result appendFormat:@"%@分", ChinaNumbers[temp]];
        else
            [result appendString:@"整"];
    }
    else
        [result appendString:@"整"];
    return result;
}


-(BOOL)checkPhoneNumInput{
    
    NSString * MOBILE = @"^1(3[0-9]|4[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";

    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    BOOL res1 = [regextestmobile evaluateWithObject:self];
    BOOL res2 = [regextestcm evaluateWithObject:self];
    BOOL res3 = [regextestcu evaluateWithObject:self];
    BOOL res4 = [regextestct evaluateWithObject:self];
    
    if (res1 || res2 || res3 || res4 ){
        return YES;
    }else{
        return NO;
    }
}

- (NSString *)chineseTransformToPinyin{
    
    //将NSString转换成NSMutableString
    NSMutableString *pinyin = [self copy];
    
    //将汉字转换成拼音(带音标)
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformToLatin, NO);
    
    //去掉拼音的音标
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformStripCombiningMarks, NO);
    
    return [pinyin stringByReplacingOccurrencesOfString:@" " withString:@""];
    
}
@end
