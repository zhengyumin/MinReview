//
//  UIView+Category.h
//  MinReview
//
//  Created by zhengyumin on 17/5/25.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Category)

- (UIViewController *)getCurrentViewController;

//隐藏键盘
- (void)hideKeyWindow;

//查找键盘
- (UIView *)findKeyboard;

//在view子视图里查找键盘
- (UIView *)findKeyboardInView:(UIView *)view;

// 当前view的截图
- (UIImage*) snapshot;
@end
