//
//  NSString+Category.h
//  MinReview
//
//  Created by zhengyumin on 17/5/19.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (Category)

+ (BOOL) isEmpty:(NSString*) str;

- (CGSize) sizeWithFont:(UIFont*) font width:(CGFloat) width;


//字符个数统计
- (int)charCounts;

//转换成大写金额
- (NSString *)ToChineseAmount;

// 检验手机号
- (BOOL)checkPhoneNumInput;

//中文转拼音
- (NSString *)chineseTransformToPinyin;
@end
