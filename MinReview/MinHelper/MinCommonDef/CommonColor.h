//
//  CommonColor.h
//  MinReview
//
//  Created by zhengyumin on 17/5/24.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#ifndef CommonColor_h
#define CommonColor_h

#import <Foundation/Foundation.h>
#import "UIColor+Extension.h"

//获取RGB实现
#define RGBA(r,g,b,a) [UIColor colorWithRed:(r)/255.0 green:(g)/255.0 blue:(b)/255.0 alpha:(a)]

#define HexRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define HexRGBAlpha(rgbValue,a) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:(a)]


#define TINTCOLOR HexRGB(0x252525)

//主页底部颜色值
#define MAIN_BOTTOM_COLOR RGBA(255, 255, 255, 1.0)
#define BACKGOUND_COLOR RGBA(240, 240, 240, 1.0)

//顶部bar颜色值
#define DEFAULT_TITLE_COLOR RGBA(123, 32, 24, 1.0)

//导航条
#define MRNavBarBgColor                     HexRGB(0xFFFFFF)
#define MRNavBarRedBgColor                  HexRGB(0xB7504C)

//背景
#define MRBGPageColor                       HexRGB(0xF1F1F1)
#define MRBGPageWhiteColor                  HexRGB(0xFFFFFF)
#define MRBGPop                             HexRGB(0x514F51)

//所有弹出框透明度变成纯黑的40%
#define MRBGAlertColor                      HexRGBAlpha(0X000000,0.4)

//文本
#define MRTextGray                          HexRGB(0X747474)
#define MRTextLightGray                     HexRGB(0xA4A4A4)
#define MRTextDarkGray                      HexRGB(0X36363D)
#define MRTextWhite                         HexRGB(0xFFFFFF)
#define MRTextRed                           HexRGB(0xCD6456)

#define MRTextLeftTitle                     HexRGB(0x666666)
#define MRTextSubTitle                      HexRGB(0xA4A4A4)

#define MRNavTitleTextColor                 HexRGB(0x36363D)
#define MRNavTitleWhiteTextColor            HexRGB(0xFFFFFF)
#define MRNavRightBtnTextColor              HexRGB(0x36363D)

//线条
#define MRLineGray                          HexRGB(0xE2E2E0)
#define MRLineGrayF1                        HexRGB(0xF1F1F1)
#define MRLineCCCCCC                        HexRGB(0xCCCCCC)

//按钮
#define MRButtonWhiteColor                  HexRGB(0xFFFFFF)
#define MRButtonWhiteHeightColor            HexRGB(0x000000)

#endif /* CommonColor_h */
