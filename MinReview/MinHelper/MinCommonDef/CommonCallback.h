//
//  CommonCallback.h
//  MinReview
//
//  Created by zhengyumin on 2017/6/29.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#ifndef CommonCallback_h
#define CommonCallback_h

typedef void (^OperatorCallback)(int code, NSString* msg); //操作返回回调 成功code＝0 其他失败
typedef void (^DataCallback)(id data); // 数据返回回调 成功不为nil

#endif /* CommonCallback_h */
