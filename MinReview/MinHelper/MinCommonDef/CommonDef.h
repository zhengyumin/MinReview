//
//  CommonDef.h
//  MinReview
//
//  Created by zhengyumin on 2017/6/29.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#ifndef CommonDef_h
#define CommonDef_h

// 配置 key定义
#define TEL             @"tel"
#define NICKNAME        @"nickname"
#define PASSWORD        @"password"

#define UID             @"uid"
#define TOKEN           @"token"
#define FIRST_LAUNCH    @"first_launch"
#define AUTO_LOGIN      @"auto_login"
#define POSITION        @"position"

#define CODE            @"code"
#define OLDPASSWORD     @"old_password"
#define LONGITUDE       @"longitude"
#define LATITUDE        @"latitude"

/**
 iOS设备宽高比
 4\4s {320, 480}  5s\5c {320, 568}  6 {375, 667}    6+ {414, 736}    iphoneX{375, 812}
 0.66           0.5634             0.562218          0.5625          0.4618
 */

//字体大小
#define FONT(obj) [UIFont systemFontOfSize:obj]

//iPhone6尺寸为基准做适配
#define IPHONE6_SCREEN_WITDTH = 375;
#define IPHONE6_SCREEN_HEIGHT = 667;

//适配宽高
#define FIT(obj) ((obj) * (SCREEN_WIDTH/375))
#define FITWIDTH(obj) ((obj) * [SCREEN_WIDTH/375])
#define FITHEIGHT(obj) ((obj) * [SCREEN_HEIGHT/667])

//获取当前系统的版本号
#define IOS_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]

//判断是否版本超过8.0
#define IOS8 ([[UIDevice currentDevice].systemVersion doubleValue]>=8.0)
#define IOS10 ([[UIDevice currentDevice].systemVersion doubleValue]>=10.0)

//获取当前语言
#define CurrentLanguage [[NSLocale preferredLanguages] objectAtIndex:0]

//底部按钮高度
#define TABBAR_HEIGHT 49

//导航栏高度
#define NAVBAR_HEIGHT 64

//状态栏尺寸,不包含热点时的多出尺寸
#define STATUS_BAR_WIDTH ([[UIApplication sharedApplication] statusBarFrame].size.width)
#define STATUS_BAR_HEIGHT ([[UIApplication sharedApplication] statusBarFrame].size.height)

//导航栏状态栏高度
#define TITLE_HEIGHT_WITH_BAR (STATUS_BAR_HEIGHT+44)

//包括状态栏的屏幕尺寸
#define SCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define SCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)

//不包括状态栏的屏幕尺寸
#define FRAME_WIDTH ([UIScreen mainScreen].applicationFrame.size.width)
#define FRAME_HEIGHT ([UIScreen mainScreen].applicationFrame.size.height)

//判断字符串是否为空
#define IS_EMPTY(str) (str == nil || [str length] == 0)

// 弱引用
#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;

//AppId
#define APP_ID @""

//官网主页
#define WEB_SITE @"http://www."

// 服务器接口根地址
#define API_HOST @"http://www.banyoubao.cn/"

#endif /* CommonDef_h */
