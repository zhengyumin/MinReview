//
//  AppDelegate.m
//  MinReview
//
//  Created by zhengyumin on 17/5/16.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "AppDelegate.h"
#import "MainTabBarController.h"
#import "LoginViewController.h"
#import "MinHelper.h"
@interface AppDelegate ()
@property(nonatomic, assign) BOOL tokenFailedTip;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [self.window setBackgroundColor:[UIColor clearColor]];
    
    LoginViewController *vc = [[LoginViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    
    [self.window setRootViewController:nav];
    [self.window makeKeyAndVisible];
    
    // token异常处理
    WS(weakSelf)
    _tokenFailedTip = NO;
    [[HttpNetwork getInstance] setCheckTokenFailed:^{
        if (weakSelf.tokenFailedTip) {
            return;
        }
        [weakSelf showTokenFailedAlert];
         weakSelf.tokenFailedTip = YES;
    }];

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (void)showTokenFailedAlert{
    WS(weakSelf);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"提示" message:@"你的账号已在其他设备上登录，若不是本人操作请及时修改密码。" preferredStyle: UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        //回到登录
        UIStoryboard* story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController* ctrl = [story instantiateViewControllerWithIdentifier:@"LoginNav"];
        self.window.rootViewController = ctrl;
        weakSelf.tokenFailedTip = NO;
    }]];
    [self.window.rootViewController presentViewController:alert animated:true completion:nil];
}
@end
