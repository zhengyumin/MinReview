//
//  MinAlertView.m
//  MinReview
//
//  Created by zhengyumin on 2017/11/24.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "MinAlertView.h"

static CGFloat const kAlertWidth = 270.0f;
static CGFloat const kAlertHeight = 160.0f;

@interface MinAlertView() {
    BOOL _isLeftClick;
}

@property (nonatomic, strong) UIView *alertWindow;

@end

@implementation MinAlertView

#pragma mark - Life Cycle
- (id)init{
    self = [super init];
    if (self) {
        _width = kAlertWidth;
        _height = kAlertHeight;
        [self addSubview:self.backgroundImageView];
        [self addSubview:self.titleLabel];
        [self addSubview:self.leftBtn];
        [self addSubview:self.rightBtn];
    }
    return self;
}

- (id)initWithTitle:(NSString *)title{
    if (self = [self init]) {
        self.titleLabel.text = title;
    }
    return self;
}

- (id)initWithTitle:(NSString *)title contentText:(NSString *)content leftButton:(NSString *)left rightButton:(NSString *)right{
    if (self = [self initWithTitle:title]) {
        CGFloat btnY;
        
        if (!IS_EMPTY(content)) {
            self.contentLabel.text = content;
            [self addSubview:self.contentLabel];
            
            CGSize size = [content boundingRectWithSize:CGSizeMake(_width - 50, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading attributes:[NSDictionary dictionaryWithObject:[UIFont systemFontOfSize:15.0f] forKey:NSFontAttributeName] context:nil].size;
            [self.contentLabel setFrame:CGRectMake(25, CGRectGetMaxY(self.titleLabel.frame) + 20, size.width, size.height)];
            btnY = 30 + CGRectGetMaxY(self.contentLabel.frame);
        }else{
            btnY = 30 + CGRectGetMaxY(self.titleLabel.frame);
        }
        
        self.leftBtn.frame = CGRectMake(0, btnY, _width/2.0f, 40);
        self.rightBtn.frame = CGRectMake(_width/2.0f, btnY, _width/2.0f, 40);
        [self.leftBtn setTitle:left forState:UIControlStateNormal];
        [self.rightBtn setTitle:right forState:UIControlStateNormal];
    }
    return self;
}

#pragma mark - Event Response
- (void)leftBtnClicked:(id)sender{
    _isLeftClick = YES;
    [self dismissAlert];
    if (self.leftBlock) {
        self.leftBlock();
    }
}

- (void)rightBtnClicked:(id)sender{
    _isLeftClick = NO;
    [self dismissAlert];
    if (self.rightBlock) {
        self.rightBlock();
    }
}

- (void)show{
    self.frame = CGRectMake((SCREEN_WIDTH - self.width) * 0.5, - self.height - 30, self.width, self.height);
    UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
    [window addSubview:self];
}

- (void)dismissAlert{
    [self removeAlert];
    if (self.dismissBlock) {
        self.dismissBlock();
    }
}

#pragma mark - Private Method
- (void)updateAlertSize{
    CGFloat alertH = CGRectGetMaxY(self.leftBtn.frame);
    self.backgroundImageView.frame = CGRectMake(0, 0, self.width, alertH);
    self.bounds = CGRectMake(0, 0, self.width, alertH);
    self.height = alertH;
}

- (void)removeAlert{
    CGRect afterFrame = CGRectMake((SCREEN_WIDTH - _width) * 0.5, SCREEN_HEIGHT + 30, _width, _height);
    
    [UIView animateWithDuration:0.35f delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.frame = afterFrame;
        if (_isLeftClick) {
            self.transform = CGAffineTransformMakeRotation(-M_1_PI / 1.5);
        }else {
            self.transform = CGAffineTransformMakeRotation(M_1_PI / 1.5);
        }
    } completion:^(BOOL finished) {
        [self.alertWindow removeFromSuperview];
        self.alertWindow = nil;
        [self removeFromSuperview];
    }];
}

- (void)willMoveToSuperview:(UIView *)newSuperview{
    if (newSuperview == nil) {
        return;
    }
    [self updateAlertSize];//更新自身尺寸
    UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
    [window addSubview:self.alertWindow];
    
    self.transform = CGAffineTransformMakeRotation(-M_1_PI / 2);
    CGRect afterFrame = CGRectMake((SCREEN_WIDTH - _width) * 0.5, (SCREEN_HEIGHT - _height) * 0.5, _width, _height);
    
    [UIView animateWithDuration:0.35f delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.transform = CGAffineTransformMakeRotation(0);
        self.frame = afterFrame;
    } completion:^(BOOL finished) {
        
    }];
    [super willMoveToSuperview:newSuperview];
}

#pragma mark - Setter and Getter
- (UIView *)alertWindow{
    if (!_alertWindow) {
        _alertWindow = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        _alertWindow.backgroundColor = [UIColor blackColor];
        _alertWindow.alpha = 0.6f;
        _alertWindow.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    }
    return _alertWindow;
}

- (UIImageView *)backgroundImageView{
    if (!_backgroundImageView) {
        _backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _width, _height)];
        _backgroundImageView.image = [[self getAlertImage:@"alert_bg.png"]resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10 , 20, 20)];
    }
    return _backgroundImageView;
}

- (UILabel *)titleLabel{
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 15, _width, 25)];
        _titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
        _titleLabel.textColor = HexRGB(0xA4A4A4);
        [_titleLabel setTextAlignment:NSTextAlignmentCenter];
        [_titleLabel setBackgroundColor:[UIColor clearColor]];
    }
    return _titleLabel;
}

- (UILabel *)contentLabel{
    if (!_contentLabel) {
        _contentLabel = [[UILabel alloc] init];
        _contentLabel.numberOfLines = 0;
        _contentLabel.textColor = HexRGB(0X747474);
        _contentLabel.font = MinAlert_FONT15;
    }
    return _contentLabel;
}

- (UIButton *)leftBtn{
    if (!_leftBtn) {
        _leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        UIImage *leftNor = [[self getAlertImage:@"alert_cancel_nor.png"]resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 20, 20)];
        UIImage *leftPress = [[self getAlertImage:@"alert_cancel_pre.png"]resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 20, 20)];
        
        [_leftBtn setBackgroundImage:leftNor forState:UIControlStateNormal];
        [_leftBtn setBackgroundImage:leftPress forState:UIControlStateHighlighted];
        [_leftBtn setBackgroundImage:leftPress forState:UIControlStateSelected];
        
        _leftBtn.titleLabel.font = MinAlert_FONT15;
        [_leftBtn setTitle:@"取消" forState:UIControlStateNormal];
        [_leftBtn setTitleColor:HexRGB(0X747474) forState:UIControlStateNormal];
        [_leftBtn addTarget:self action:@selector(leftBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _leftBtn;
}

- (UIButton *)rightBtn{
    if (!_rightBtn) {
        _rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        
        UIImage *rightNor = [[self getAlertImage:@"alert_ok_nor.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 20, 20)];
        UIImage *rightPress = [[self getAlertImage:@"alert_ok_pre.png"]resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 20, 20)];
        
        [_rightBtn setBackgroundImage:rightNor forState:UIControlStateNormal];
        [_rightBtn setBackgroundImage:rightPress forState:UIControlStateHighlighted];
        [_rightBtn setBackgroundImage:rightPress forState:UIControlStateSelected];
        
        _rightBtn.titleLabel.font = MinAlert_FONT15;
        [_rightBtn setTitle:@"确定" forState:UIControlStateNormal];
        [_rightBtn setTitleColor:HexRGB(0X747474) forState:UIControlStateNormal];
        [_rightBtn addTarget:self action:@selector(rightBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _rightBtn;
}

- (UIImage *)getAlertImage:(NSString *)imgName{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"MinAlertView" ofType:@"bundle"];
    return [UIImage imageWithContentsOfFile:[path stringByAppendingPathComponent:imgName]];
}

@end

