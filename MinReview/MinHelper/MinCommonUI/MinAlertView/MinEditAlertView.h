//
//  MinEditAlertView.h
//  MinReview
//
//  Created by zhengyumin on 2017/11/24.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "MinAlertView.h"

@interface MinEditAlertView : MinAlertView

@property(nonatomic, copy) void(^editor)(NSString* text); //返回参数为编辑后的内容

- (instancetype)initWithTitle:(NSString *)title;
- (instancetype)initWithTitle:(NSString *)title tip:(NSString*) tip;


- (void)setNumOnly:(BOOL)numOnly; //只能输入数字
- (void)setTextValue:(NSString*)textValue; //指定编辑前的内容
- (void)setTextValue:(NSString*)textValue PlaceHolder:(NSString*)placeholder; //指定编辑前的内容和无内容时的提示信息

@end
