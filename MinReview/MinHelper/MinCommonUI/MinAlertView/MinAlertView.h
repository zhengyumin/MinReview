//
//  MinAlertView.h
//  MinReview
//
//  Created by zhengyumin on 2017/11/24.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonDef.h"
#import "CommonColor.h"
#define MinAlert_FONT15 [UIFont boldSystemFontOfSize:15]

@interface MinAlertView : UIView

@property(nonatomic, assign) CGFloat width;
@property(nonatomic, assign) CGFloat height;
@property(nonatomic, strong) UIImageView* backgroundImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIButton *leftBtn;
@property (nonatomic, strong) UIButton *rightBtn;

@property(nonatomic, copy) dispatch_block_t leftBlock;
@property(nonatomic, copy) dispatch_block_t rightBlock;
@property(nonatomic, copy) dispatch_block_t dismissBlock;

- (id)initWithTitle:(NSString *)title;

- (id)initWithTitle:(NSString *)title
        contentText:(NSString *)content
         leftButton:(NSString *)leftTitle
        rightButton:(NSString *)rigthTitle;

- (void)leftBtnClicked:(id)sender;
- (void)rightBtnClicked:(id)sender;
- (void)show;
- (void)dismissAlert;
- (void)updateAlertSize;
@end
