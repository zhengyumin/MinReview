//
//  MinEditAlertView.m
//  MinReview
//
//  Created by zhengyumin on 2017/11/24.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "MinEditAlertView.h"

@interface MinEditAlertView()<UITextFieldDelegate>

@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UILabel *tipLabel;
@property (nonatomic, strong) UILabel *moneyLabel;
@end

@implementation MinEditAlertView

- (id)init {
    self = [super init];
    if (self) {
        // 输入框
        [self addSubview:self.textField];
    }
    return self;
}

- (instancetype)initWithTitle:(NSString*)title{
    if (self = [self init]) {
        // 标题
        self.titleLabel.text = title;
        [self.textField setFrame:CGRectMake(15, CGRectGetMaxY(self.titleLabel.frame)+10, self.width-30, 35)];
        
        CGFloat btnY = 30 + CGRectGetMaxY(self.textField.frame);
        self.leftBtn.frame = CGRectMake(0, btnY, self.width/2.0f, 40);
        self.rightBtn.frame = CGRectMake(self.width/2.0f, btnY, self.width/2.0f, 40);
        [self updateAlertSize];
    }
    return self;
}

- (instancetype) initWithTitle:(NSString *)title tip:(NSString *)tip {
    if (self = [self initWithTitle:title]) {
        // 提示语
        if (!IS_EMPTY(tip)) {
            self.tipLabel.text = tip;
            [self.tipLabel setFrame:CGRectMake(15, CGRectGetMaxY(self.titleLabel.frame)+10, self.width-30, 20)];
            [self addSubview:self.tipLabel];
            
            [self.textField setFrame:CGRectMake(15, CGRectGetMaxY(self.tipLabel.frame)+10, self.width-30, 35)];
            
            CGFloat btnY = 30 + CGRectGetMaxY(self.textField.frame);
            self.leftBtn.frame = CGRectMake(0, btnY, self.width/2.0f, 40);
            self.rightBtn.frame = CGRectMake(self.width/2.0f, btnY, self.width/2.0f, 40);
            [self updateAlertSize];
        }
    }
    return self;
}

#pragma mark - Event Response
- (void) leftBtnClicked:(id)sender {
    [super leftBtnClicked:sender];
    [[[UIApplication sharedApplication] keyWindow]endEditing:YES];
}

- (void)rightBtnClicked:(id)sender {
    [super rightBtnClicked:sender];
    if (self.editor) {
        self.editor(self.textField.text);
    }
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}

- (void)show {
    [self.textField becomeFirstResponder];
    [super show];
}

#pragma mark - Setter and Getter
- (UITextField *)textField{
    if (!_textField) {
        _textField = [[UITextField alloc] init];
        _textField.borderStyle = UITextBorderStyleRoundedRect;
        _textField.layer.cornerRadius = 5;
        _textField.layer.masksToBounds = YES;
        _textField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _textField.delegate = self;
        [_textField setBackgroundColor:[UIColor whiteColor]];
        [_textField setTextColor:HexRGB(0X747474)];
        [_textField setFont:MinAlert_FONT15];
    }
    return _textField;
}

- (UILabel *)tipLabel{
    if (!_tipLabel) {
        _tipLabel = [[UILabel alloc] init];
        _tipLabel.font = [UIFont systemFontOfSize:14];
        _tipLabel.textColor = [UIColor darkGrayColor];
        _tipLabel.backgroundColor = [UIColor clearColor];
    }
    return _tipLabel;
}

- (void)setNumOnly:(BOOL)numOnly {
    if(numOnly) {
        _textField.keyboardType = UIKeyboardTypeNumberPad;
    }
}

- (void)setTextValue:(NSString*)textValue {
    self.textField.text = textValue;
}

- (void)setTextValue:(NSString*)textValue PlaceHolder:(NSString*)placeholder {
    self.textField.text = textValue;
    self.textField.placeholder = placeholder;
}


@end
