//
//  LFPictureCycleScrollView.h
//  LongFor
//
//  Created by apple on 2017/8/3.
//
//

#import <UIKit/UIKit.h>

@interface CycleScrollView : UIView
@property (nonatomic, strong) NSArray *imageArray;//保存图片的数组
@property (nonatomic, strong) UIImageView *centerImageView;//显示中间图片的控件
- (void)setImageByIndex:(long)currentIndex;
- (void)refreshImage;//刷新图片
@end

