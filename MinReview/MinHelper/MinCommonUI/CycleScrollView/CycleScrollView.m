//
//  CycleScrollView.m
//  LongFor
//
//  Created by apple on 2017/8/3.
//
//

#import "CycleScrollView.h"
#import "UIImageView+WebCache.h"

#define imageX  0
#define imageY  0
#define imageW  self.frame.size.width
#define imageH  self.frame.size.height
#define kImageCount self.imageArray.count
#define kTimeChange 3.0f

@interface CycleScrollView ()<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *scrollView;/**< 滚动视图控件 */
@property (nonatomic, strong) UIPageControl *pageControl;/**< 页码指示视图控件 */
@property (nonatomic, strong) NSTimer *timer; /**< 定时器 */
@property (nonatomic, strong) UIImageView *leftImageView;/**< 显示左边图片的控件 */
@property (nonatomic, strong) UIImageView *rightImageView; /**< 显示右边图片的控件*/
@property (nonatomic, assign) long currentIndex;  /**< 图片当前的下标索引*/
@property (nonatomic, assign) BOOL isTimeUp;

@end

@implementation CycleScrollView

- (id)initWithFrame:(CGRect)frame{
    if ([super initWithFrame:frame]){
        [self initUI];
    }
    return self;
}

#pragma mark - InitUI
- (void)initUI{
    _currentIndex = 0;
    _isTimeUp = NO;
    _imageArray = @[@"h0",@"h1",@"h2",@"h3",@"h4"];
    [self addSubview:self.scrollView];
    [self addSubview:self.pageControl];
    [self.scrollView addSubview:self.leftImageView];
    [self.scrollView addSubview:self.centerImageView];
    [self.scrollView addSubview:self.rightImageView];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:kTimeChange target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
    [self setImageByIndex:self.currentIndex];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self refreshImage];
    self.scrollView.contentOffset = CGPointMake(imageW, 0);
    self.pageControl.currentPage = self.currentIndex ;
    //手动控制图片滚动应该取消那个三秒的计时器
    if (!_isTimeUp){
       [_timer setFireDate:[NSDate dateWithTimeIntervalSinceNow:kTimeChange]];
    }
    _isTimeUp = NO;
}

#pragma mark - Private Methods
//计时器到时,系统滚动图片
- (void)timerAction{
    [self.scrollView setContentOffset:CGPointMake(imageW * 2, 0) animated:YES];
    _isTimeUp = YES;
    [NSTimer scheduledTimerWithTimeInterval:0.4f target:self selector:@selector(scrollViewDidEndDecelerating:) userInfo:nil repeats:NO];
}

//刷新图片
- (void)refreshImage{
    if (self.scrollView.contentOffset.x > imageW){
        self.currentIndex = ((self.currentIndex + 1) % kImageCount);
    }else if(self.scrollView.contentOffset.x < imageW){
        self.currentIndex = ((self.currentIndex - 1 + kImageCount) % kImageCount);
    }
    [self setImageByIndex:self.currentIndex];
}

//根据传回的下标设置三个ImageView的图片
- (void)setImageByIndex:(long)currentIndex{
    //判断是图片数组还是URL数组
//    if ([[_imageArray objectAtIndex:0] isKindOfClass:[NSString class]]) {
//        [self.centerImageView sd_setImageWithURL:[NSURL URLWithString:[_imageArray objectAtIndex:currentIndex]]];
//        [self.leftImageView sd_setImageWithURL:[NSURL URLWithString:[_imageArray objectAtIndex:((self.currentIndex - 1 + kImageCount) % kImageCount)]]];
//        [self.rightImageView sd_setImageWithURL:[NSURL URLWithString:[_imageArray objectAtIndex:((self.currentIndex + 1) % kImageCount)]]];
//    }else if([[_imageArray objectAtIndex:0] isKindOfClass:[UIImage class]]){
//        self.centerImageView.image = [_imageArray objectAtIndex:currentIndex];
//        self.leftImageView.image = [_imageArray objectAtIndex:((self.currentIndex - 1 + kImageCount) % kImageCount)];
//        self.rightImageView.image = [_imageArray objectAtIndex:((self.currentIndex + 1) % kImageCount)];
//    }
    
    NSString *curruntImageName = [NSString stringWithFormat:@"h%ld",currentIndex];
    self.centerImageView.image = [UIImage imageNamed:curruntImageName];
    self.leftImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"h%ld",((self.currentIndex - 1 + kImageCount) % kImageCount)]];
    self.rightImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"h%ld",((self.currentIndex + 1) % kImageCount)]];
    self.pageControl.currentPage = currentIndex;
}

#pragma mark - Setter and Getter

- (void)setImageArray:(NSArray *)imageArray{
    _imageArray = imageArray;
}

- (UIScrollView *)scrollView{
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(imageX, imageY,imageW , imageH)];
        _scrollView.contentOffset = CGPointMake(imageW, 0);
        _scrollView.contentSize = CGSizeMake(imageW * 3, 0);
        _scrollView.pagingEnabled = YES;
        _scrollView.delegate = self;
        _scrollView.bounces = NO;
        _scrollView.showsHorizontalScrollIndicator = NO;
    }
    return _scrollView;
}

- (UIPageControl *)pageControl{
    if (!_pageControl) {
        _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(imageX, imageY, imageW, 30)];
        _pageControl.center = CGPointMake(imageW / 2, imageH / 5 * 4);
        _pageControl.currentPageIndicatorTintColor = [UIColor purpleColor];//当前点的颜色
        _pageControl.pageIndicatorTintColor = [UIColor whiteColor];//其他点的颜色
        _pageControl.enabled = NO;
        _pageControl.numberOfPages = kImageCount;//page数
    }
    return _pageControl;
}

- (UIImageView *)leftImageView{
    if (!_leftImageView) {
        _leftImageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageX, imageY, imageW, imageH)];
    }
    return _leftImageView;
}

- (UIImageView *)centerImageView{
    if (!_centerImageView) {
        _centerImageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageW, imageY, imageW, imageH)];
    }
    return _centerImageView;
}

- (UIImageView *)rightImageView{
    if (!_rightImageView) {
        _rightImageView = [[UIImageView alloc] initWithFrame:CGRectMake(imageW *2, imageY, imageW, imageH)];
    }
    return _rightImageView;
}

@end
