//
//  MinPickerView.h
//  MinReview
//
//  Created by zhengyumin on 2017/11/27.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CommonDef.h"
#import "CommonColor.h"

@interface MinPickerView : UIView
@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) UIDatePicker *datePicker;
- (instancetype)initWithIsLandscape:(BOOL)landscape;

- (void)selectAtIndex:(NSInteger)index;
- (void)selectString:(NSString *)string;
- (void)selectRow:(NSInteger)row inComponent:(NSInteger)component;

- (void)showDatePickerWithDate:(NSDate *)date confirmBlock:(void (^)(NSDate *date))confirmBlock;

- (void)showPickerViewWithDataSource:(id)dataSource delegate:(id)delegate finish:(void (^)())confirmBlock;

- (void)showPickerViewWithStrings:(NSArray *)strings confirmBlock:(void (^)(NSInteger index))confirmBlock;
@end
