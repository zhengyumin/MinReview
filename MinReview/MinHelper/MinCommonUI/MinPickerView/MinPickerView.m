//
//  MinPickerView.m
//  MinReview
//
//  Created by zhengyumin on 2017/11/27.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "MinPickerView.h"

@interface MinPickerView () <UIPickerViewDataSource, UIPickerViewDelegate>
@property (nonatomic, strong) UIToolbar *toolbar;
@property (nonatomic, strong) NSArray *strings;
@property (nonatomic, assign) BOOL landscape;
@property (nonatomic, copy) void (^pickerViewBlock)(NSInteger index);
@property (nonatomic, copy) void (^finishBlock)();
@property (nonatomic, copy) void (^datePickerBlock)(NSDate *date);
@end

@implementation MinPickerView

- (instancetype)initWithIsLandscape:(BOOL)landscape {
    self = [super init];
    if (self) {
        self.landscape = landscape;
        [self setupView];
    }
    return self;
}
- (void)setupView {
    [self setBackgroundColor:[UIColor colorWithWhite:0 alpha:0.8]];
    if (self.landscape) {
        self.transform = CGAffineTransformMakeRotation(M_PI_2);
        self.frame = CGRectMake(0, 0, SCREEN_HEIGHT, SCREEN_WIDTH);
    } else {
        self.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    }
    
}


- (void)selectAtIndex:(NSInteger)index {
    if ([self.pickerView numberOfRowsInComponent:0] > index) {
        [self.pickerView selectRow:index inComponent:0 animated:YES];
    }
}

- (void)selectString:(NSString *)string {
    [self.strings enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        if ([obj isEqualToString:string]) {
            [self.pickerView selectRow:idx inComponent:0 animated:YES];
            *stop = YES;
        }
    }];
    
}

- (void)selectRow:(NSInteger)row inComponent:(NSInteger)component {
    [self.pickerView selectRow:row inComponent:component animated:YES];
}

- (void)showPickerViewWithStrings:(NSArray *)strings confirmBlock:(void (^)(NSInteger index))confirmBlock {
    self.strings = strings;
    self.pickerViewBlock = confirmBlock;
    
    [self addSubview:self.pickerView];
    [self addSubview:self.toolbar];
    UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
    [window addSubview:self];
    [self showPickerView];
}

- (void)showPickerViewWithDataSource:(id)dataSource delegate:(id)delegate finish:(void (^)())confirmBlock {
    [self addSubview:self.pickerView];
    [self addSubview:self.toolbar];
    self.pickerView.dataSource = dataSource;
    self.pickerView.delegate = delegate;
    self.finishBlock = confirmBlock;
    UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
    [window addSubview:self];
    [self showPickerView];
}


- (void)showDatePickerWithDate:(NSDate *)date confirmBlock:(void (^)(NSDate *date))confirmBlock {
    
    self.datePickerBlock = confirmBlock;
    [self addSubview:self.datePicker];
    if (date) {
        self.datePicker.date = date;
    }
    [self addSubview:self.toolbar];
    UIWindow *window = [[UIApplication sharedApplication].windows lastObject];
    [window addSubview:self];
    [self showPickerView];
}
#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    
    return self.strings.count;
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component  {
    return self.strings[row];
}

#pragma mark - Event Response

- (void)finishItemClicked:(UIBarButtonItem *)item {
    if (self.datePickerBlock) {
        self.datePickerBlock(self.datePicker.date);
    }
    if (self.pickerViewBlock) {
        self.pickerViewBlock([self.pickerView selectedRowInComponent:0]);
    }
    if (self.finishBlock) {
        self.finishBlock();
    }
    [self hidePickerView];
}

- (void)cancelItemClicked:(UIBarButtonItem *)item {
    [self hidePickerView];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self hidePickerView];
}

#pragma mark - private method

- (void)showPickerView {
    self.toolbar.frame = CGRectMake(0, self.bounds.size.height, self.bounds.size.width, 44.0f);
    if (_datePicker) {
        self.datePicker.frame = CGRectMake(0, self.bounds.size.height + 44.0f, self.bounds.size.width, 216.0f);
    } else {
        self.pickerView.frame = CGRectMake(0, self.bounds.size.height + 44.0f, self.bounds.size.width, 216.0f);
    }
    [UIView animateWithDuration:0.3f animations:^{
        if (_datePicker) {
            self.datePicker.frame = CGRectMake(0, self.bounds.size.height - 216.0f, self.bounds.size.width, 216.0f);
        } else {
            self.pickerView.frame = CGRectMake(0, self.bounds.size.height - 216.0f, self.bounds.size.width, 216.0f);
        }
        self.toolbar.frame = CGRectMake(0, self.bounds.size.height - 260.0f, self.bounds.size.width, 44.0f);
    }];
}

- (void)hidePickerView {
    [UIView animateWithDuration:0.3f animations:^{
        if (_datePicker) {
            self.datePicker.frame = CGRectMake(0, self.bounds.size.height + 44.0f, self.bounds.size.width, 216.0f);
        } else {
            self.pickerView.frame = CGRectMake(0, self.bounds.size.height + 44.0f, self.bounds.size.width, 216.0f);
        }
        self.toolbar.frame = CGRectMake(0, self.bounds.size.height, self.bounds.size.width, 44.0f);
    } completion:^(BOOL finished) {
        [self finish];
    }];
    
}

- (void)finish {
    [self removeFromSuperview];
}

#pragma mark - Setter and Getter
- (UIDatePicker *)datePicker {
    if (!_datePicker) {
        _datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, self.bounds.size.height + 44.0f, self.bounds.size.width, 216.0f)];
        [_datePicker setDatePickerMode:UIDatePickerModeDate];
        [_datePicker setValue:HexRGB(0x4c4c4c) forKey:@"textColor"];
        _datePicker.date = [NSDate date];
        [_datePicker setBackgroundColor:[UIColor whiteColor]];
    }
    return _datePicker;
}

- (UIPickerView *)pickerView {
    if (!_pickerView) {
        _pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, self.frame.size.height + 44, self.frame.size.width, 216)];
        [_pickerView setValue:HexRGB(0x4c4c4c) forKey:@"textColor"];
        _pickerView.delegate = self;
        _pickerView.dataSource = self;
        [_pickerView setBackgroundColor:[UIColor whiteColor]];
    }
    return _pickerView;
}

- (UIToolbar *)toolbar {
    if (!_toolbar) {
        _toolbar = [[UIToolbar alloc] init];
        _toolbar.barTintColor = [UIColor whiteColor];
        _toolbar.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, 44);
        UIBarButtonItem *finishItem = [[UIBarButtonItem alloc] initWithTitle:@"确认" style:UIBarButtonItemStylePlain target:self action:@selector(finishItemClicked:)];
        [finishItem setTintColor:HexRGB(0x4c4c4c)];
        UIBarButtonItem *cancelItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStylePlain target:self action:@selector(cancelItemClicked:)];
        [cancelItem setTintColor:HexRGB(0x4c4c4c)];
        UIBarButtonItem *spaceItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
        UIBarButtonItem *leftSpaceItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        leftSpaceItem.width = FIT(15);
        UIBarButtonItem *rightSpaceItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        rightSpaceItem.width = FIT(15);
        _toolbar.items = @[leftSpaceItem,cancelItem,spaceItem,finishItem,rightSpaceItem];
    }
    return _toolbar;
}
@end
