//
//  MinTool.h
//  MinReview
//
//  Created by zhengyumin on 2017/11/22.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MinTool : NSObject

/**
 * 系统版本号
 */
+ (CGFloat)iOSSystemVersion;

/**
 * 系统时间
 */
+ (NSDate *)curretnSystemDate;

/**
 删除空格
 @param string 原字符串
 @return 删除空格后的字符串
 */
+ (NSString *)deletWhiteSpace:(NSString *)string;

/**
 中文转拼音
 @param chinese 中文
 @return 拼音
 */
+ (NSString *)transform:(NSString *)chinese;

/**
 转换成MD5字符串
 */
+ (NSString *)getMD5String:(NSString *)str;

/**
 获取本地存储目录
 @param pathName  指定目录
 @param forCach   缓存路径
 @return 根路径
 */
+ (NSString *)getLocalPath:(NSString*)pathName ForCache:(BOOL)forCach;

/**
 *  json转字典
 *  @param jsonString json字符串
 *  @return 字典
 */
+ (NSDictionary *)jsonToDictionary:(NSString *)jsonString;

/**
 获取顶层控制器
 @return 顶层控制器
 */
+ (UIViewController *)getAppRootViewController;
@end
