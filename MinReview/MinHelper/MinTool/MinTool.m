//
//  MinTool.m
//  MinReview
//
//  Created by zhengyumin on 2017/11/22.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "MinTool.h"
#import <CommonCrypto/CommonDigest.h>

@implementation MinTool

/**
 * 系统版本号
 */
+ (CGFloat)iOSSystemVersion {
    NSString *systemVersion = [UIDevice currentDevice].systemVersion;
    NSArray *versions = [systemVersion componentsSeparatedByString:@"."];
    if (versions.count > 1) {
        systemVersion = [NSString stringWithFormat:@"%@.%@", versions[0], versions[1]];
    }
    else {
        systemVersion = versions[0];
    }
    return [systemVersion floatValue];
}

/**
 * 系统时间
 */
+ (NSDate *)curretnSystemDate {
    NSDate *date = [NSDate date];//获得时间对象
    NSTimeZone *zone = [NSTimeZone systemTimeZone];//获得系统时区
    NSTimeInterval time = [zone secondsFromGMTForDate:date];//以秒为单位返回当前时间与系统格林尼治时间的差
    NSDate *dateNow = [date dateByAddingTimeInterval:time];//然后把差的时间加上，就是当前系统的准确时间
    return dateNow;
}

/**
 删除空格
 @param string 原字符串
 @return 删除空格后的字符串
 */

+ (NSString *)deletWhiteSpace:(NSString *)string {
    if (nil == string) {
        return string;
    }
    NSCharacterSet *set = [NSCharacterSet whitespaceAndNewlineCharacterSet];
    return [string stringByTrimmingCharactersInSet:set];
}

/**
 中文转拼音
 @param chinese 中文
 @return 拼音
 */
+ (NSString *)transform:(NSString *)chinese{
    //将NSString装换成NSMutableString
    NSMutableString *pinyin = [chinese mutableCopy];
    
    //将汉字转换为拼音(带音标)
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformMandarinLatin, NO);
    
    //去掉拼音的音标
    CFStringTransform((__bridge CFMutableStringRef)pinyin, NULL, kCFStringTransformStripCombiningMarks, NO);
    //    NSLog(@"%@", pinyin);
    
    //返回最近结果
    return [pinyin stringByReplacingOccurrencesOfString:@" " withString:@""];
}

/**
 转换成MD5字符串
 */
+ (NSString *)getMD5String:(NSString *)str {
    const char *cstr = [str UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cstr, (CC_LONG)strlen(cstr), digest);
    NSMutableString *result = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH*2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++){
        [result appendFormat:@"%02X",digest[i]];
    }
    return result;
}

/**
获取本地存储目录 指定目录
 */
+ (NSString*)getLocalPath:(NSString*)pathName ForCache:(BOOL)forCache {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(forCache?NSCachesDirectory:NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:pathName];
    if (![[NSFileManager defaultManager] fileExistsAtPath:path]) {
        [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
    }
    return path;
}

/**
 *  json转字典
 *  @param jsonString json字符串
 *  @return 字典
 */
+ (NSDictionary *)jsonToDictionary:(NSString *)jsonString {
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *jsonError;
    NSDictionary *resultDic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableLeaves error:&jsonError];
    return resultDic;
}

/**
 获取顶层窗口
 @return 顶层窗口
 */
+ (UIWindow *)getAppTopWindow{
    return [[UIApplication sharedApplication].windows lastObject];
}


/**
 是否为纯数字
 @param str 字符串
 @return 布尔值
 */
- (BOOL)isNumberString:(NSString *)str{
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"0123456789."] invertedSet]; //invertedSet 方法是去反字符,把所有的除了数字的字符都找出来
    
    NSString *filtered = [[str componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];  //componentsSeparatedByCharactersInSet 方法是把输入框输入的字符string 根据cs中字符一个一个去除cs字符并分割成单字符并转化为 NSArray, 然后componentsJoinedByString 是把NSArray 的字符通过 ""无间隔连接成一个NSString字符 赋给filtered.就是只剩数字了.
    
    BOOL basicTest = [str isEqualToString:filtered];
    return basicTest;
}
@end
