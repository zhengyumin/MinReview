//
//  AppRouter.h
//  MinReview
//
//  Created by zhengyumin on 2017/6/29.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MinHelper.h"

@interface AppRouter : NSObject
// 跳转到主页
+ (void) jumpMain:(UIViewController*) controller;

// 跳转景点列表
+ (void)pushLandscapeList:(UIViewController *)controller;

// 跳转发现列表
+ (void)pushDcvyList:(UIViewController *)controller;
@end

