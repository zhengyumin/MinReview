//
//  AppRouter.m
//  MinReview
//
//  Created by zhengyumin on 2017/6/29.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "AppRouter.h"
#import "AppDelegate.h"
#import "MainTabBarController.h"
#import "LandscapeListController.h"
#import "DisvoveryListController.h"

@implementation AppRouter
+ (void)jumpMain:(UIViewController *)controller {
    MainTabBarController *ctrl = [MainTabBarController shareInstance];
    AppDelegate* app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    app.window.rootViewController = ctrl;
    [app.window.layer transitionWithAnimType:TransitionAnimTypeRamdom subType:TransitionSubtypesFromRamdom curve:TransitionCurveRamdom duration:0.5f];
}

// 跳转景点列表
+ (void)pushLandscapeList:(UIViewController *)controller{
    LandscapeListController * ctrl = [[LandscapeListController alloc] init];
    ctrl.hidesBottomBarWhenPushed = YES;
    [controller.navigationController pushViewController:ctrl animated:YES];
    
}

// 跳转发现列表
+ (void)pushDcvyList:(UIViewController *)controller{
    DisvoveryListController * ctrl = [[DisvoveryListController alloc] init];
    ctrl.hidesBottomBarWhenPushed = YES;
    [controller.navigationController pushViewController:ctrl animated:YES];
}
@end
