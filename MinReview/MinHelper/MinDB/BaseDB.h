//
//  BaseDB.h
//  MinReview
//
//  Created by zhengyumin on 17/5/25.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+LKDBHelper.h"

@interface BaseDB : NSObject
+ (LKDBHelper*) getUsingLKDBHelper;
@end
