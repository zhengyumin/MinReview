//
//  BaseDB.m
//  MinReview
//
//  Created by zhengyumin on 17/5/25.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "BaseDB.h"
#import "NSObject+LKModel.h"

@implementation BaseDB

+ (LKDBHelper*) getUsingLKDBHelper {
    
    static LKDBHelper* helper;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        // 拷贝本地数据库到沙盒 //dbcentiwu
        NSFileManager* fileMgr = [NSFileManager defaultManager];
        NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString* document = [paths objectAtIndex:0];
        NSString* dbfile = [document stringByAppendingString:@"/db/hischool.db"];
        NSString* path = [document stringByAppendingPathComponent:@"db"];
        NSLog(@"dbfile-%@",dbfile);
        [[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:nil];
        if (![fileMgr fileExistsAtPath:dbfile]) {
            NSString* src = [[NSBundle mainBundle] pathForResource:@"hischool" ofType:@"db"];
            NSLog(@"src-%@",src);
            NSError* error;
            BOOL success = [fileMgr copyItemAtPath:src
                                            toPath:dbfile
                                             error:&error];
            if (!success) {
                NSLog(@"拷贝数据库失败, 错误描述:%@", [error localizedDescription]);
            }
        }
        
        // 数据库操作对象实例化
        helper = [[LKDBHelper alloc] init];
        [helper setDBName:@"hikvison"];
    });
    return helper;
}

@end
