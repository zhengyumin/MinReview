//
//  CXZBaseTableViewController.h
//  MinReview
//
//  Created by zhengyumin on 17/5/19.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MJRefresh.h"

@interface MinBaseTableViewController : UITableViewController

@property (nonatomic, assign) BOOL preScrollEnable;
@property (nonatomic, assign) BOOL preInterEnable;
@property (nonatomic, assign) CGFloat moveY;

//键盘处理
- (void)registerHideKeyWindow;
- (void)hideKeyWindow;

//返回、主题、下一步按钮初始化
- (void) setupBack;
- (void) setupBackNew;
- (void) setupNextWithImage:(UIImage*) image;
- (UIButton*) setupNextWithString:(NSString*) text;
- (void) setupTitleWithString:(NSString *)text withColor:(UIColor *)color;

// 回调  子类继承各自处理
- (void)onBack;   //返回
- (void)onNext;  //下一步
- (void)popByDrag;  //侧拉返回的回调
- (BOOL)needDragBack; //是否需要侧拉返回
- (BOOL)needRegisterHideKeyboard;  //是否需要自动隐藏键盘
- (BOOL)gestureRecognizer:(UIGestureRecognizer*)gestureRecognizer shouldReceiveTouch:(UITouch*)touch;

// 进度效果
- (void)showLoading;
- (void)hideLoading;

@end
