//
//  CXZBaseViewController.h
//  MinReview
//
//  Created by zhengyumin on 17/5/19.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MinBaseViewController : UIViewController
//返回、主题、下一步按钮初始化
- (void) registerHideKeyWindow;
- (void) setupBack;
- (void) setupBackWithImage:(UIImage*) image withString:(NSString*) text;
- (void) setupNextWithImage:(UIImage*) image;
- (void) setupNextWithString:(NSString*) text;
- (void) setupTitleWithString:(NSString*) text withColor:(UIColor*) color;

// 回调  子类继承各自处理
- (void) onBack;
- (void) onNext;
- (void) popByDrag;
- (BOOL) needDragBack;
- (BOOL) needRegisterHideKeyboard;
- (void) hideKeyWindow;

// 进度效果
- (void) showLoading;
- (void) hideLoading;
@end
