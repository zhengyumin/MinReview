//
//  CXZBaseViewController.m
//  MinReview
//
//  Created by zhengyumin on 17/5/19.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "MinBaseViewController.h"
#import "MinHelper.h"
#import "MJRefresh.h"
#import "NSString+Category.h"
#import "MONActivityIndicatorView.h"
#import "Masonry.h"

@interface MinBaseViewController () <UITextFieldDelegate, UIGestureRecognizerDelegate, MONActivityIndicatorViewDelegate> {
}

@property (nonatomic, strong) MONActivityIndicatorView* loadingView;/**<指示器>*/


@end

@implementation MinBaseViewController

#pragma mark - UIViewController life cycle
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad{
    [super viewDidLoad];
    [self registerHideKeyWindow];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

#pragma mark - Event Response
//重载onback
- (void)onBack {
    [self.view endEditing:YES];
    UIViewController * controller = [self.navigationController.viewControllers objectAtIndex:0];
    if(controller == self) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

//重载onnext
- (void)onNext {
    
}

#pragma mark - UIGestureRecognizerDelegate
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"] || [NSStringFromClass([touch.view class]) isEqualToString:@"UICollectionViewCellContentView"]) {
        return NO;
    }
    if ( [NSStringFromClass([touch.view.superview class]) isEqualToString:@"UICollectionViewCell"]) {
        return NO;
    }
    NSLog(@"%@", NSStringFromClass([touch.view class]));
    return YES;
}

#pragma mark - Private Methods
//初始化标题栏、返回
- (void)setupBack{
    UIButton* back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame = CGRectMake(0, 0, 20, 20);
    // [back setTitle:@"返回" forState:UIControlStateNormal];
    // back.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];

    //  [back setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 40)];
    //  [back setTitleEdgeInsets:UIEdgeInsetsMake(0, -8, 0, 0)];
 
    self.navigationItem.leftBarButtonItem = [self setupItemWithImage:[UIImage imageNamed:@"back"] action:@selector(onBack)];
}

- (UIBarButtonItem *)setupItemWithImage:(UIImage *)image action:(SEL)action {
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
    [btn setImage:image forState:UIControlStateNormal];
    [btn addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:btn];
    return item;
}

- (UIBarButtonItem *)setupItemWithText:(NSString *)text action:(SEL)action {
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithTitle:text style:UIBarButtonItemStylePlain target:self action:action];
    [barButtonItem setTitleTextAttributes:@{NSFontAttributeName:FONT(MAIN_TEXT_SIZE),NSForegroundColorAttributeName:color} forState:UIControlStateNormal];
    return barButtonItem;
}

- (void)setupBackWithImage:(UIImage *)image withString:(NSString *)text {
    UIButton* back = [UIButton buttonWithType:UIButtonTypeCustom];
    CGSize sz = [text sizeWithFont:[UIFont boldSystemFontOfSize:14] width:100];
    back.frame = CGRectMake(0, 0, sz.width+20, 44);
    [back setTitle:text forState:UIControlStateNormal];
    back.titleLabel.font = [UIFont boldSystemFontOfSize:14];
    [back setImage:image forState:UIControlStateNormal];
    [back setTitleEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 0)];
    [back addTarget:self action:@selector(onBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* leftItem = [[UIBarButtonItem alloc] initWithCustomView:back];
    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)setupNextWithImage:(UIImage *)image {
    UIButton* back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.frame = CGRectMake(0, 0, 44, 44);
    [back setImage:image forState:UIControlStateNormal];
    [back addTarget:self action:@selector(onNext) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* rightItem = [[UIBarButtonItem alloc] initWithCustomView:back];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)setupNextWithString:(NSString *)text {
    UIButton* back = [UIButton buttonWithType:UIButtonTypeCustom];
    back.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    CGSize size = [text sizeWithFont:[UIFont systemFontOfSize:16] width:100];
    back.frame = CGRectMake(0, 0, size.width, 44);
    [back setTitle:text forState:UIControlStateNormal];
    [back setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [back addTarget:self action:@selector(onNext) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem* rightItem = [[UIBarButtonItem alloc] initWithCustomView:back];
    self.navigationItem.rightBarButtonItem = rightItem;
}

- (void)setupTitleWithString:(NSString *)text withColor:(UIColor *)color{
    UILabel *titleView = [[UILabel alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2.0-30, 0, 60, NAVBAR_HEIGHT)];
    titleView.textAlignment = NSTextAlignmentCenter;
    titleView.text = text;
    titleView.font = [UIFont boldSystemFontOfSize:17];
    titleView.textColor = color;
    self.navigationItem.titleView = titleView;
}

//界面点击 关闭键盘用
- (void)registerHideKeyWindow {
    if([self needRegisterHideKeyboard]) {
        //        UITapGestureRecognizer * tap=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hideKeyWindow)];
        //        tap.delegate = self;
        //        [self.view addGestureRecognizer:tap];
    }
}

- (void)hideKeyWindow {
    [[[UIApplication sharedApplication] keyWindow]endEditing:YES];
}

//重载popByDrag
- (void)popByDrag {
}

// 是否需要侧拉返回
- (BOOL)needDragBack {
    return YES;
}

//是否需要注册隐藏键盘手势
- (BOOL)needRegisterHideKeyboard {
    return YES;
}
// 界面效果
- (void)showLoading {
    if (self.loadingView == nil) {
        self.loadingView = [[MONActivityIndicatorView alloc] init];
        self.loadingView.delegate = self;
        self.loadingView.numberOfCircles = 5;
        self.loadingView.radius = 10;
        self.loadingView.internalSpacing = 3;
        self.loadingView.duration = 0.75;
        [self.view addSubview:self.loadingView];
        [self.loadingView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(self.view);
        } ];
    }
    self.loadingView.hidden = NO;
    [self.loadingView startAnimating];
}

- (void)hideLoading {
    if (self.loadingView) {
        [self.loadingView stopAnimating];
        [self.loadingView setHidden:YES];
    }
}

- (UIColor *)activityIndicatorView:(MONActivityIndicatorView *)activityIndicatorView
      circleBackgroundColorAtIndex:(NSUInteger)index {
    CGFloat red   = (arc4random() % 256)/255.0;
    CGFloat green = (arc4random() % 256)/255.0;
    CGFloat blue  = (arc4random() % 256)/255.0;
    CGFloat alpha = 1.0f;
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

@end
