//
//  CXZBaseNavigationController.m
//  MinReview
//
//  Created by zhengyumin on 2017/8/10.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "MinBaseNavigationController.h"
#import "MinHelper.h"

@interface MinBaseNavigationController ()

@end

@implementation MinBaseNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationBar lt_setBackgroundColor:HexRGB(0x29bc61)];
    
    self.navigationBar.barTintColor = HexRGB(0x29bc61);
    self.navigationBar.translucent = NO;
    self.navigationBar.titleTextAttributes = @{NSFontAttributeName:[UIFont systemFontOfSize:17],NSForegroundColorAttributeName:[UIColor whiteColor]};
    [self.navigationBar setBackgroundImage:[UIImage imageWithColor:HexRGB(0x29bc61)] forBarMetrics:UIBarMetricsDefault];
    [self.navigationBar setShadowImage:[UIImage imageWithColor:HexRGB(0x29bc61)]];
}

@end
