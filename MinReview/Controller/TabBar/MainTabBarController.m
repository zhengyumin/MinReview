//
//  MainTabBarController.m
//  MinReview
//
//  Created by zhengyumin on 17/5/18.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "MainTabBarController.h"
#import "KBRoundedButton.h"
#import "PathView.h"

@interface MainTabBarController ()<PathViewDelegate>

@property (nonatomic, strong) PathView *pathView;/**<中心按钮点击后展开的动画图层>*/
@property (nonatomic, strong) NSArray *itemArray;/**<动画图层上的子控件数组>*/

@end

@implementation MainTabBarController

+ (MainTabBarController *)shareInstance{
    static MainTabBarController *s_instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        s_instance = [MainTabBarController new];
    });
    return s_instance;
}

#pragma mark - UIViewController life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
}

#pragma mark - initUI
- (void)initUI{
    [self setupChildController];//初始化子控制器
    [self setupTabBar];//初始化tabBar
    [self configTabBar];//配置tabBar
    [self setupTabbarCenterButton];//添加tabBar主按钮
    [self setupPathView];//添加动画图层
}


#pragma mark - PathViewDelegate
- (void)itemButtonTappedAtIndex:(NSUInteger)index{
    //
}

#pragma mark - Event Response
-(void)clickBtn:(UIButton *)btn{
    self.selectedIndex = 0;
    _pathView.hidden = NO;
    [self.view bringSubviewToFront:_pathView];
    [_pathView centerButtonTapped];
}

#pragma mark - Private Methods
/**
 初始化子控制器
 */
-(void)setupChildController{
    NSArray *vcArray = @[@"LandscapeController",@"AddNewPointController",@"DiscoveryController"];
    for (int i = 0; i < vcArray.count; i++) {
        NSString *className = vcArray[i];
        UIViewController *vc = [[NSClassFromString(className) alloc] init];
        MinBaseNavigationController *nav = [[MinBaseNavigationController alloc] initWithRootViewController:vc];
        [self addChildViewController:nav];
    }
}

/**
 初始化tabBar
 */
- (void)setupTabBar{
    NSArray *nImages = @[@"ic_tab_seek",@"zixuan_ic_n",@"ic_tab_info"];
    NSArray *hImages = @[@"ic_tab_seek_select",@"zixuan_ic_h",@"ic_tab_info_select"];
    NSArray *tabbarTitles = @[@"景点",@"",@"发现"];
    for (int i = 0; i < nImages.count; i++) {
        UITabBarItem *item = [UITabBarItem new];
        item.selectedImage = [[UIImage imageNamed:hImages[i]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        item.image = [[UIImage imageNamed:nImages[i]] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:HexRGB(0x39bd00),NSForegroundColorAttributeName,nil] forState:UIControlStateSelected];
        [item setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:HexRGB(0x666666), NSForegroundColorAttributeName,nil] forState:UIControlStateNormal];
        item.title = tabbarTitles[i];
        UINavigationController *nav = self.childViewControllers[i];
        nav.tabBarItem = item;
    }
}


/**
 配置tabBar
 */
- (void)configTabBar{
    self.tabBar.barTintColor = [UIColor whiteColor];
    self.tabBar.translucent = NO;
  
    //去除tabBar横线
    CGRect rect = CGRectMake(0, 0, self.view.frame.size.width, 1);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [[UIColor clearColor] CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [self.tabBar setBackgroundImage:img];
    [self.tabBar setShadowImage:img];
}

/**
 创建tabBar主按钮
 */
- (void)setupTabbarCenterButton{
    KBRoundedButton *btn = [KBRoundedButton buttonWithType:UIButtonTypeCustom];
    UIImage * normalImage = [UIImage imageNamed:@"ic_tab_add"];
    UIImage * selectedImage = [UIImage imageNamed:@"ic_tab_add_select"];
    [btn setImage:normalImage forState:UIControlStateNormal];
    [btn setImage:selectedImage forState:UIControlStateSelected];
    btn.frame = CGRectMake(0.0, 0.0, normalImage.size.width-3, normalImage.size.height-3);
    [btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchDown];
    btn.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    btn.selected = NO;
    btn.shadowEnabled = YES;
    //添加按钮圆形背景
    CGPoint center = self.tabBar.center;
    center.y = 10;
    CGFloat btnW = (normalImage.size.width + 8);
    CGFloat radius = btnW/2;
    UIView *background = [[UIView alloc] initWithFrame:CGRectMake(0, 0, btnW, btnW)];
    background.backgroundColor = [UIColor whiteColor];
    background.layer.cornerRadius = radius;
    background.clipsToBounds = YES;
    background.center = center;
    btn.center = CGPointMake(radius, radius);
    [background addSubview:btn];
    [self.tabBar addSubview:background];
    UITabBarItem *item = self.tabBar.items[1];
    item.enabled = NO;
}

- (void)setupPathView{
    PathView *pathView = [[PathView alloc] initWithCenterImage:[UIImage imageNamed:@"ic_tab_add"] hilightedImage:[UIImage imageNamed:@"ic_tab_add_select"]];
    _pathView = pathView;
    pathView.delegate = self;
    [self.view addSubview:_pathView];
    NSArray *imageArray = @[@"ic_seek_sort_learn",@"ic_seek_sort_sport",@"ic_seek_sort_movie",@"ic_seek_sort_travel",@"ic_seek_sort_traffic",@"ic_seek_sort_boardgame",@"ic_seek_sort_party",@"ic_seek_sort_meal",@"ic_seek_sort_friend",@"ic_seek_sort_gametogether",@"ic_seek_sort_others"];
    for (int i = 0; i < imageArray.count; i++) {
        PathItemButton *itemButton = [[PathItemButton alloc] initWithImage:[UIImage imageNamed:imageArray[i]] highlightedImage:[UIImage imageNamed:imageArray[i]]
                                                          backgroundImage:[UIImage imageNamed:imageArray[i]]
                                               backgroundHighlightedImage:[UIImage imageNamed:imageArray[i]]];
        itemButton.tag = i;
        [_pathView addPathItem:itemButton];
    }
    _pathView.hidden = YES;
}


@end
