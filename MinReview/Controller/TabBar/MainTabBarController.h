//
//  MainTabBarController.h
//  MinReview
//
//  Created by zhengyumin on 17/5/18.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MinHelper.h"

@interface MainTabBarController : UITabBarController

+ (MainTabBarController*) shareInstance;

@end
