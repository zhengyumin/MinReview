//
//  LandscapeController.m
//  MinReview
//
//  Created by zhengyumin on 17/5/19.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "LandscapeController.h"
#import "MinHelper.h"
#import "MainTabBarController.h"

@interface LandscapeController ()

@property(nonatomic,strong)   NSArray *dateArray;
@end

@implementation LandscapeController

#pragma mark - UIViewController life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark - initUI
- (void)initUI {
    [self setupTitleWithString:@"经典景点" withColor:[UIColor whiteColor]];
   // [self.navigationController.navigationBar lt_setBackgroundColor:HexRGB(0x29bc61)];
    [self setupNextWithString:@"列表"];
     self.view.backgroundColor = BACKGOUND_COLOR;
}

- (void)onNext {
    [AppRouter pushLandscapeList:self];
}

@end
