//
//  DiscoveryController.m
//  MinReview
//
//  Created by zhengyumin on 17/5/19.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "MinHelper.h"
#import "DiscoveryController.h"
#import "MainTabBarController.h"
#import "MinAlertView.h"
#import "MinEditAlertView.h"
@interface DiscoveryController ()

@property(nonatomic,strong)   NSArray *dateArray;
@property (nonatomic ,strong) UIButton *locationButton;

@end

@implementation DiscoveryController

#pragma mark - UIViewController life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
   
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    MinAlertView* alert = [[MinAlertView alloc] initWithTitle:@"温馨提示" contentText:nil leftButton:@"取消" rightButton:@"确定"];
//    
//    alert.leftBlock = ^() {
//        
//    };
//    [alert show];
    
    MinEditAlertView* alert = [[MinEditAlertView alloc] initWithTitle:@"昵称" tip:nil];
    [alert setTextValue:@"" PlaceHolder:@"请输入昵称"];
    [alert setEditor:^(NSString * nickName) {
        if (IS_EMPTY(nickName)) {
            [MBProgressHUD showMessageAutoHide:@"昵称不能为空" toView:nil];
            return ;
        }
    }];
    [alert show];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark - initUI
- (void)initUI {
    [self setupTitleWithString:@"自发景点" withColor:[UIColor whiteColor]];
    //[self.navigationController.navigationBar lt_setBackgroundColor:HexRGB(0x29bc61)];
    [self setupNextWithString:@"列表"];
    _dateArray = [NSArray array];
   
   
}

- (void)onNext{
    [AppRouter pushDcvyList:self];
}

@end
