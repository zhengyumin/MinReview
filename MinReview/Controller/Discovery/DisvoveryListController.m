//
//  DisvoveryListController.m
//  MinReview
//
//  Created by zhengyumin on 17/5/26.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "DisvoveryListController.h"
#import "MinHelper.h"
#import "LandscapeCell.h"

@interface DisvoveryListController (){
    BOOL hasMore;
    int page;
}

@property(nonatomic,strong) NSMutableArray *dataArray;
@end

@implementation DisvoveryListController

#pragma mark - UIViewController life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
}

#pragma mark - InitUI
- (void)initUI {
    [self setupBack];
    [self setupTitleWithString:@"发现景点" withColor:[UIColor whiteColor]];
    self.view.backgroundColor = BACKGOUND_COLOR;
    _dataArray = [NSMutableArray array];
    __weak typeof(self) weakSelf = self;
    self.tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weakSelf requestWithMore:NO];
    }];
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weakSelf requestWithMore:YES];
    }];
    [self.tableView.mj_header beginRefreshing];
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _dataArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LandscapeCell *cell = [LandscapeCell cellWithTableView:tableView];
    cell.viewcontroller = self;
    [cell setViewspot:_dataArray[indexPath.section]];
    return cell;
}

#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (UIView*) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView* root = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 10)];
    [root setBackgroundColor:[UIColor clearColor]];
    return root;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 100;
}

- (void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    UIView* line = [[UIView alloc] initWithFrame:CGRectMake(0, cell.frame.size.height-1, cell.frame.size.width, 1)];
    [line setBackgroundColor:HexRGB(0xE1E0E0)];
    [cell addSubview:line];
}

#pragma mark - Private Methods
- (void)requestWithMore:(BOOL)more{
    if(more){
        if(hasMore){
            page++;
        }else{
            [self.tableView.mj_footer endRefreshingWithNoMoreData];
            return;
        }
    }else{
        page = 1;
        hasMore = YES;
    }
    GetViewspotCmd* cmd = [[GetViewspotCmd alloc] init];
    cmd.act = @"spot2";
    cmd.page = page;
    
    __weak typeof(self) weakSelf = self;
    [MBProgressHUD showMessage:@"" toView:nil];
    [[HttpNetwork getInstance] requestWithAFN:cmd success:^(BaseRespond *respond) {
        [weakSelf.tableView.mj_header endRefreshing];
        [weakSelf.tableView.mj_footer endRefreshing];
        NSArray *array = [Viewspot mj_objectArrayWithKeyValuesArray:respond.data];
        if (!more) {
            [_dataArray removeAllObjects];
        }
        if (array.count < 10) {
            hasMore = NO;
            [weakSelf.tableView.mj_footer endRefreshingWithNoMoreData];
        } else {
            hasMore = YES;
        }
        if (array.count > 0) {
            [_dataArray addObjectsFromArray:array];
            //[weakSelf.tableView hideView];
        }else{
            //[weakSelf.tableView createNoDataWithFrame:weakSelf.tableView.frame];
        }
        [weakSelf.tableView reloadData];
        [MBProgressHUD hideHUDForView:nil animated:NO];
    } failed:^(BaseRespond *respond, NSString *error) {
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:error toView:nil];
    }];
}

@end
