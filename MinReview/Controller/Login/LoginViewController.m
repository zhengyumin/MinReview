//
//  LoginViewController.m
//  MinReview
//
//  Created by zhengyumin on 17/5/24.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "LoginViewController.h"
#import "UIImage+Resize.h"
#import "MinHelper.h"

@interface LoginViewController()<CAAnimationDelegate>{
    CGFloat _keyboardHeight;
    CGFloat _changeHeight;
}


@property (strong, nonatomic) NSTimer* timer;
@property (weak, nonatomic)  UIImageView *hiStr;
@property (weak, nonatomic)  UIImageView *xiaoyuanStr;

@property (weak, nonatomic)  UIView *telView;
@property (weak, nonatomic)  UIImageView *telImageView;
@property (weak, nonatomic)  UITextField *telField;
@property (weak, nonatomic)  UIView *telLine;

@property (weak, nonatomic)  UIView *pwdView;
@property (weak, nonatomic)  UIImageView *pwdImageView;
@property (weak, nonatomic)  UITextField *pwdField;
@property (weak, nonatomic)  UIView *pwdLine;

@property (weak, nonatomic)  UIImageView *bgImage;
@property (weak, nonatomic)  UIButton *codeBtn;
@property (weak, nonatomic)  UIButton *loginBtn;

@end

@implementation LoginViewController {
    
}

#pragma mark - UIViewController life cycle
-(void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self addAnimation];
    _telField.text = [ConfigUtil stringWithKey:TEL];
    _pwdField.text = [ConfigUtil stringWithKey:PASSWORD];
}

- (void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [self layoutSubviews];
}

#pragma mark - InitUI
- (void)initUI {
    UIImageView *background = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_login"]];
    _bgImage = background;
    [self.view addSubview:_bgImage];
    
    //添加logo
    UIImageView *hikImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_login_logo"]];
    _hiStr = hikImageView;
    [self.view addSubview:_hiStr];
    
    UIImageView *visionStrImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_login_xiaoyuan"]];
    _xiaoyuanStr = visionStrImageView;
    [self.view addSubview:_xiaoyuanStr];
    
    //手机号输入框
    UIView *telView = [[UIView alloc] init];
    _telView = telView;
    UIImageView *telImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_login_phone"]];
    _telImageView = telImageView;
    UITextField *telField = [[UITextField alloc] init];
    _telField = telField;
    
    _telField.placeholder = @"请输入手机号";
    _telField.textColor = [UIColor whiteColor];
    _telField.font = [UIFont systemFontOfSize:15];
    [_telField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    _telField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [_telView addSubview:_telImageView];
    [_telView addSubview:_telField];
    [self.view addSubview:_telView];
    
    //密码输入框
    UIView *pwdView = [[UIView alloc] init];
    _pwdView = pwdView;
    UIImageView *pwdImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ic_login_password"]];
    _pwdImageView = pwdImageView;
    
    UITextField *pwdField = [[UITextField alloc] init];
    _pwdField = pwdField;
    _pwdField.placeholder = @"请输入密码";
    _pwdField.textColor = [UIColor whiteColor];
    _pwdField.font = [UIFont systemFontOfSize:15];
    [_pwdField setValue:[UIColor whiteColor] forKeyPath:@"_placeholderLabel.textColor"];
    _pwdField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [_pwdView addSubview:_pwdImageView];
    [_pwdView addSubview:_pwdField];
    [self.view addSubview:_pwdView];
    
    //登录按钮
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _loginBtn = loginBtn;
    [_loginBtn setTitle:@"登录" forState:UIControlStateNormal];
    _loginBtn.titleLabel.font = [UIFont boldSystemFontOfSize:15];
    _loginBtn.layer.cornerRadius = 22;
    _loginBtn.clipsToBounds = YES;
    _loginBtn.layer.backgroundColor = [HexRGB(0x29bc61) colorWithAlphaComponent:0.5].CGColor;
    [_loginBtn addTarget:self action:@selector(loginBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:_loginBtn];
    
    //分割线
    UIView *telLine = [[UIView alloc] init];
    _telLine = telLine;
    [_telLine setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_telLine];
    UIView *pwdLine = [[UIView alloc] init];
    _pwdLine = pwdLine;
    [_pwdLine setBackgroundColor:[UIColor whiteColor]];
    [self.view addSubview:_pwdLine];
    
    [self.navigationController.navigationBar setBackgroundColor:[UIColor whiteColor]];
    self.navigationController.navigationBar.hidden = YES;
    _telField.text = [ConfigUtil stringWithKey:TEL];
    BOOL autoLogin = [ConfigUtil boolWithKey:AUTO_LOGIN];
    if(autoLogin) {
        //  [MinReviewUtils jumpMain:self];
    }
    //增加监听，当键盘出现或改变时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    //增加监听，当键退出时收出消息
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

#pragma mark - LayoutSubviews
- (void)layoutSubviews{
    [_bgImage mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.mas_equalTo(self.view).insets(UIEdgeInsetsMake(-40,-40,-40,-40));
    }];
    [_hiStr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(80, 80));
        make.centerX.mas_equalTo(self.view.mas_centerX).offset(-65);
        make.centerY.mas_equalTo(self.view.mas_top).offset(142);
    }];
    
    [_xiaoyuanStr mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(150, 70));
        make.centerX.mas_equalTo(self.view.mas_centerX).offset(55);
        make.centerY.mas_equalTo(self.view.mas_top).offset(144);
    }];
    //手机号
    [_telView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.top.mas_equalTo(self.view.mas_top).offset(200);
        make.height.mas_equalTo(43);
        
    }];
    [_telImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.left.mas_equalTo(_telView.mas_left).offset(20);
        make.top.mas_equalTo(_telView.mas_top).offset(12);
    }];
    [_telField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_telView.mas_left).offset(48);
        make.right.mas_equalTo(_telView.mas_right).offset(-30);
        make.top.mas_equalTo(_telView.mas_top).offset(7);
        make.height.mas_equalTo(30);
    }];
    [_telLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_telView.mas_left).offset(40);
        make.right.mas_equalTo(_telView.mas_right).offset(-20);
        make.top.mas_equalTo(_telView.mas_bottom).offset(8);
        make.height.mas_equalTo(1);
        
    }];
    //密码框及自控件
    [_pwdView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left);
        make.right.mas_equalTo(self.view.mas_right);
        make.top.mas_equalTo(self.view.mas_top).offset(265);
        make.height.mas_equalTo(43);
        
    }];
    [_pwdImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(20, 20));
        make.left.mas_equalTo(_pwdView.mas_left).offset(20);
        make.top.mas_equalTo(_pwdView.mas_top).offset(14);
    }];
    [_pwdField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_pwdView.mas_left).offset(48);
        make.right.mas_equalTo(_pwdView.mas_right).offset(-30);
        make.top.mas_equalTo(_pwdView.mas_top).offset(7);
        make.height.mas_equalTo(30);
    }];
    [_pwdLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(_telView.mas_left).offset(40);
        make.right.mas_equalTo(_telView.mas_right).offset(-20);
        make.top.mas_equalTo(_pwdView.mas_bottom).offset(7);
        make.height.mas_equalTo(1);
    }];
    [_loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self.view.mas_left).offset(20);
        make.right.mas_equalTo(self.view.mas_right).offset(-20);
        make.top.mas_equalTo(_pwdView.mas_bottom).offset(50);
        make.height.mas_equalTo(44);
    }];
}

#pragma mark - CAAnimationDelegate
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    NSString *value = [anim valueForKey:@"name"];
    if ([value isEqualToString:@"imageMov"]) {
        CALayer *layer = [anim valueForKey:@"layer"];
        [anim setValue:nil forKey:@"layer"];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 2.0*NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            [self bgImageAnimation:layer];
        });
    }
}

#pragma mark - Event Response
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [_telField resignFirstResponder];
    [_pwdField resignFirstResponder];
}

- (void)loginBtnClick:(id)sender {

    if(IS_EMPTY(_telField.text)) {
        [MBProgressHUD showMessageAutoHide:@"请输入手机号" toView:nil];
        [self performSelector:@selector(focusAccount) withObject:nil afterDelay:0.75];
        return ;
    }
    if(IS_EMPTY(_pwdField.text)) {
        [MBProgressHUD showMessageAutoHide:@"请输入密码" toView:nil];
        [self performSelector:@selector(focusPassword) withObject:nil afterDelay:0.75];
        return ;
    }
    
    [[UIApplication sharedApplication].keyWindow hideKeyWindow];
    // 执行登录请求
    LoginRequest* login = [[LoginRequest alloc] init];
    login.act = @"login";
    login.tel = _telField.text;
    login.pwd = _pwdField.text;
    [self requestLogin:login];
}

//当键盘出现或改变时调用
- (void)keyboardWillShow:(NSNotification *)aNotification {
    //获取键盘的高度
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    _keyboardHeight = kbSize.height;
    _changeHeight = CGRectGetMaxY(_loginBtn.frame)- (SCREEN_HEIGHT - _keyboardHeight) + 10;
    if(_changeHeight > 0){
        [UIView animateWithDuration:0.2 animations:^{
            CGRect frame = self.view.frame;
            frame.origin.y = -_changeHeight;
            self.view.frame = frame;
        }];
    }
    
}

//当键退出时调用
- (void)keyboardWillHide:(NSNotification *)aNotification {
    [UIView animateWithDuration:0.2 animations:^{
        CGRect frame = self.view.frame;
        frame.origin.y = 0;
        self.view.frame = frame;
    }];
}

#pragma mark - Private Methods
-(void)addAnimation {
    //定义一个动画组1
    CAAnimationGroup *group = [[CAAnimationGroup alloc] init];
    group.duration = 0.8f;
    group.fillMode = kCAFillModeBackwards;
    
    // 单体动画1
    CABasicAnimation *flyRight = [CABasicAnimation animationWithKeyPath:@"position.x"];
    flyRight.fromValue = @(-self.view.frame.size.width/2);
    flyRight.toValue = @(self.view.frame.size.width/2);
    
    // 单体动画2
    CABasicAnimation *fadeFieldIn = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fadeFieldIn.fromValue = @(0.25);
    fadeFieldIn.toValue = @(1.0);
    
    // 单体动画3
    CABasicAnimation *flyLeft = [CABasicAnimation animationWithKeyPath:@"position.x"];
    flyLeft.fromValue = @(SCREEN_WIDTH+_codeBtn.frame.size.width);
    flyLeft.toValue = @(SCREEN_WIDTH - _codeBtn.frame.size.width);
    
    [group setAnimations:@[flyRight,fadeFieldIn]];//组合动画
    group.beginTime = CACurrentMediaTime() + 0.5;
    [_telView.layer addAnimation:group forKey:nil];
    [_telLine.layer addAnimation:group forKey:nil];
    
    group.beginTime = CACurrentMediaTime() + 0.7;
    [_pwdView.layer addAnimation:group forKey:nil];
    [_pwdLine.layer addAnimation:group forKey:nil];
    
    group.beginTime = CACurrentMediaTime() + 0.9;
    [_loginBtn.layer addAnimation:group forKey:nil];
    
    group.beginTime = CACurrentMediaTime() + 1.8;
    [group setAnimations:@[flyLeft,fadeFieldIn]];
    [_codeBtn.layer addAnimation:group forKey:nil];
    
    //定义一个动画组2
    CAAnimationGroup *groupAnimation = [[CAAnimationGroup alloc] init];
    groupAnimation.duration = 0.5f;
    groupAnimation.fillMode = kCAFillModeBackwards;
    groupAnimation.beginTime = CACurrentMediaTime() + 0.5;
    groupAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    // 单体动画2.1
    CABasicAnimation *scaleDown = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    scaleDown.fromValue = @(3.5);
    scaleDown.toValue = @(1.0);
    
    // 单体动画2.2
    CABasicAnimation *rotate = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotate.fromValue = @(M_PI_4);
    rotate.toValue = @(0.0);
    
    // 单体动画2.3
    CABasicAnimation *fade = [CABasicAnimation animationWithKeyPath:@"opacity"];
    fade.fromValue = @(0.0);
    fade.toValue = @(1.0);
    
    // 单体动画2.4
    CABasicAnimation *rotate1 = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
    rotate1.fromValue = @(-M_PI_4);
    rotate1.toValue = @(0.0);
    
    [groupAnimation setAnimations:@[scaleDown, rotate, fade]];//组合动画1
    [_hiStr.layer addAnimation:groupAnimation forKey:nil];
    
    [groupAnimation setAnimations:@[scaleDown, rotate1, fade]];//组合动画2
    [_xiaoyuanStr.layer addAnimation:groupAnimation forKey:nil];
    
     [self bgImageAnimation:_bgImage.layer];

}

- (void)bgImageAnimation:(CALayer *)layer{
    CGPoint endPoint = CGPointMake(_bgImage.center.x, _bgImage.center.y);
    CGPoint farPoint = CGPointMake(_bgImage.center.x +40, _bgImage.center.y);
    CAKeyframeAnimation *movingAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    //创建移动路径
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, endPoint.x, endPoint.y);
    CGPathAddLineToPoint(path, NULL, farPoint.x, farPoint.y);
    CGPathAddLineToPoint(path, NULL, endPoint.x, endPoint.y);
    
    movingAnimation.path = path;
    movingAnimation.keyTimes = @[@(0.0), @(0.5), @(1.0)];
    movingAnimation.duration = 15.0f;
    CGPathRelease(path);
    movingAnimation.delegate = self;
    [movingAnimation setValue:layer forKey:@"layer"];
    [movingAnimation setValue:@"imageMov" forKey:@"name"];
    [layer addAnimation:movingAnimation forKey:nil];
}

//指定焦点
- (void)focusAccount {
    [_telField becomeFirstResponder];
}

- (void)focusPassword {
    [_pwdField becomeFirstResponder];
}

//网络消息
- (void) requestLogin:(BaseCommand*)cmd {
    [MBProgressHUD showMessage:@"正在登录" toView:nil];
    [[HttpNetwork getInstance] requestWithAFN:cmd success:^(BaseRespond *respond) {
        LoginReturn *loginReturn = [LoginReturn mj_objectWithKeyValues:respond.data];
        [ConfigUtil saveString:_telField.text withKey:TEL];
        [ConfigUtil saveString:_pwdField.text withKey:PASSWORD];
        [ConfigUtil saveInt:loginReturn.user_id withKey:UID];
        [ConfigUtil saveString:loginReturn.user_name withKey:NICKNAME];
        [ConfigUtil saveBool:YES withKey:AUTO_LOGIN];
        [MBProgressHUD hideHUDForView:nil animated:NO];
        [AppRouter jumpMain:self];
    } failed:^(BaseRespond *respond, NSString *error) {
        [MBProgressHUD hideHUDForView:self.view animated:NO];
        [MBProgressHUD showError:error toView:nil];
    }];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillChangeFrameNotification object:nil];
}
@end
