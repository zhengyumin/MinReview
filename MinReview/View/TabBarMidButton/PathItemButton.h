//
//  TabBarMidImageView.h
//  MinReview
//
//  Created by zhengyumin on 17/5/22.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PathItemButton;

@protocol PathItemButtonDelegate <NSObject>

- (void)itemButtonTapped:(PathItemButton *)itemButton;

@end

@interface PathItemButton : UIImageView

@property (assign, nonatomic) NSUInteger index;
@property (weak, nonatomic) id <PathItemButtonDelegate> delegate;

- (id)initWithImage:(UIImage *)image
   highlightedImage:(UIImage *)highlightedImage
    backgroundImage:(UIImage *)backgroundImage
backgroundHighlightedImage:(UIImage *)backgroundHighlightedImage;

@end
