//
//  PathView.m
//  MinReview
//
//  Created by zhengyumin on 17/5/23.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "PathView.h"
#import "PathCenterButton.h"
#import "MinHelper.h"

@interface PathView ()<PathCenterButtonDelegate, PathItemButtonDelegate,CAAnimationDelegate>

@property (strong, nonatomic) UIImage *centerImage;
@property (strong, nonatomic) UIImage *centerHighlightedImage;

@property (assign, nonatomic, getter = isBloom) BOOL bloom;
@property (assign, nonatomic) CGSize foldedSize;/**<合拢时尺寸>*/
@property (assign, nonatomic) CGSize bloomSize;/**<绽放后尺寸>*/
@property (assign, nonatomic) CGPoint expandCenter;/**<绽放后主按钮中心位置>*/

@property (strong, nonatomic) UIView *bottomView;
@property (strong, nonatomic) PathCenterButton *pathCenterButton;
@property (strong, nonatomic) NSMutableArray *itemButtons;

//声音设置
@property (assign, nonatomic) SystemSoundID bloomSound;
@property (assign, nonatomic) SystemSoundID foldSound;
@property (assign, nonatomic) SystemSoundID selectedSound;


@end

@implementation PathView

#pragma mark - life cycle
- (id)initWithCenterImage:(UIImage *)centerImage hilightedImage:(UIImage *)centerHighlightedImage{
    if (self = [super init]) {
        self.centerImage = centerImage;
        self.centerHighlightedImage = centerHighlightedImage;
        [self initUI];
    }
    return self;
}

#pragma mark - InitUI
- (void)initUI{
    self.itemButtonImages = [[NSMutableArray alloc]init];
    self.itemButtonHighlightedImages = [[NSMutableArray alloc]init];
    self.itemButtons = [[NSMutableArray alloc]init];
    
    [self setupViewsLayout];
    [self setupPathCenterButton];
    [self setupBottomView];
    [self setupSounds];
}

#pragma mark - CAAnimationDelegate
- (void)animationDidStart:(CAAnimation *)anim{
    self.userInteractionEnabled = NO;
}

-(void)animationDidStop:(CAAnimation *)foldAnimation finished:(BOOL)flag{
    self.userInteractionEnabled = YES;
    
}

#pragma mark - Event Response
- (void)centerButtonTapped{
    if (self.userInteractionEnabled) {
        self.isBloom? [self showFoldAnimaton] : [self showBloomAnimaton];
    }
}

- (void)itemButtonTapped:(PathItemButton *)itemButton{
    if ([_delegate respondsToSelector:@selector(itemButtonTappedAtIndex:)]) {
        PathItemButton *selectedButton = self.itemButtons[itemButton.tag];
        AudioServicesPlaySystemSound(self.selectedSound);
        // 选中的按钮实现动画
        [UIView animateWithDuration:0.0618f * 5
                         animations:^{
                             selectedButton.transform = CGAffineTransformMakeScale(3, 3);
                             selectedButton.alpha = 0.0f;
                         } completion:^(BOOL finished) {
                             [_delegate itemButtonTappedAtIndex:itemButton.tag];
                         }];
        
        // 未选中的按钮实现消失动画
        for (int i = 0; i < self.itemButtons.count; i++) {
            if (i == selectedButton.tag) {
                continue;
            }
            PathItemButton *unselectedButton = self.itemButtons[i];
            [UIView animateWithDuration:0.0618f * 2
                             animations:^{
                                 unselectedButton.transform = CGAffineTransformMakeScale(0, 0);
                             }];
        }
        [self hideBottomView];
        [self resetCenterBtn];
        [self resetFoldInfo];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self showFoldAnimaton];
}

#pragma mark - Private Methods
/**
 初始化尺寸
 */
- (void)setupViewsLayout{
    self.foldedSize = self.centerImage.size;
    self.bloomSize = [UIScreen mainScreen].bounds.size;
    self.bloom = NO;
    self.bloomRadius = 155.0f;
    self.foldCenter = CGPointMake(self.bloomSize.width / 2, self.bloomSize.height - 39.0f);
    self.bloomCenter = CGPointMake(self.bloomSize.width / 2, self.bloomSize.height / 2);
    self.frame = CGRectMake(0, 0, self.foldedSize.width, self.foldedSize.height);
    self.center = self.foldCenter;
}

/**
 初始化中心按钮
 */
- (void)setupPathCenterButton{
    
    _pathCenterButton = [[PathCenterButton alloc] initWithImage:self.centerImage highlightedImage:self.centerHighlightedImage];
    _pathCenterButton.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    _pathCenterButton.delegate = self;
    [self addSubview:_pathCenterButton];
    
}

/**
 初始化底部背景
 */
- (void)setupBottomView{
    
    _bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bloomSize.width, self.bloomSize.height)];
    _bottomView.backgroundColor = [UIColor blackColor];
    _bottomView.alpha = 0.0f;
    
}

/**
 初始化声音效果
 */
- (void)setupSounds{
    NSString *bloomSoundPath = [[NSBundle mainBundle]pathForResource:@"bloom" ofType:@"caf"];
    NSURL *bloomSoundURL = [NSURL fileURLWithPath:bloomSoundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)bloomSoundURL, &_bloomSound);
    
    NSString *foldSoundPath = [[NSBundle mainBundle]pathForResource:@"fold" ofType:@"caf"];
    NSURL *foldSoundURL = [NSURL fileURLWithPath:foldSoundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)foldSoundURL, &_foldSound);
    
    NSString *selectedSoundPath = [[NSBundle mainBundle]pathForResource:@"selected" ofType:@"caf"];
    NSURL *selectedSoundURL = [NSURL fileURLWithPath:selectedSoundPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)selectedSoundURL, &_selectedSound);
}

/**
 根据半径和角度计算按钮位置
 */
- (CGPoint)createEndPointWithRadius:(CGFloat)itemExpandRadius andAngel:(CGFloat)angel{
    NSLog(@"___andAngel___%f",angel);
    NSLog(@"___sinf(angel * M_PI)___%f",sinf(angel * M_PI));
    NSLog(@"___cosf(angel * M_PI)___%f",cosf(angel * M_PI));
    return CGPointMake(self.expandCenter.x - cosf(angel * M_PI) * itemExpandRadius,
                       self.expandCenter.y - sinf(angel * M_PI) * itemExpandRadius);
}

#pragma mark - 按钮折叠动画
/**
 隐藏底部背景
 */
- (void)hideBottomView{
    
    [UIView animateWithDuration:0.1f
                          delay:0.35f
                        options:UIViewAnimationOptionCurveLinear
                     animations:^{
                         _bottomView.alpha = 0.0f;
                     }
                     completion:^(BOOL finished) {
                         [self.bottomView removeFromSuperview];
                     }];
}

/**
 恢复主按钮角度
 */
- (void)resetCenterBtn{
    [UIView animateWithDuration:0.0618f * 3
                          delay:0.0618f * 2
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _pathCenterButton.transform = CGAffineTransformMakeRotation(0);
                     }
                     completion:nil];
}


/**
 重置折叠状态信息
 */
- (void)resetFoldInfo{
   
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        for (PathItemButton *itemButton in self.itemButtons) {
            [itemButton performSelector:@selector(removeFromSuperview)];
        }
        
        self.frame = CGRectMake(0, 0, self.foldedSize.width, self.foldedSize.height);
        self.center = self.foldCenter;
        self.pathCenterButton.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
        
        self.hidden = YES;
    });
    
    _bloom = NO;
}

/**
 移除子按钮标签
 */
- (void)resetItemLabel{
    for (UILabel *label in self.subviews){
        if ([label isKindOfClass:[UILabel class]]) {
            [label removeFromSuperview];
        }
    }
}

/**
 实现折叠组合动画
 */
- (CAAnimationGroup *)foldAnimationFromPoint:(CGPoint)endPoint withFarPoint:(CGPoint)farPoint{
    
    // 1.旋转动画
    CAKeyframeAnimation *rotationAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.values = @[@(0), @(M_PI), @(M_PI * 2)];
    rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    rotationAnimation.duration = 0.35f;
    
    // 2.平移动画
    CAKeyframeAnimation *movingAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    
    //创建移动路径
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, endPoint.x, endPoint.y);
    CGPathAddLineToPoint(path, NULL, farPoint.x, farPoint.y);
    CGPathAddLineToPoint(path, NULL, self.expandCenter.x, self.expandCenter.y);
    
    movingAnimation.keyTimes = @[@(0.0f), @(0.75), @(1.0)];
    movingAnimation.path = path;
    movingAnimation.duration = 0.35f;
    CGPathRelease(path);
    
    // 3.组合动画
    CAAnimationGroup *animations = [CAAnimationGroup animation];
    animations.animations = @[rotationAnimation, movingAnimation];
    animations.duration = 0.35f;
    
    return animations;
}

/**
 重置子按钮动画
 */
- (void)resetItemBtnAnimation{
    for (int i = 1; i <= self.itemButtons.count; i++) {
        
        PathItemButton *itemButton = self.itemButtons[i - 1];
        
        CGFloat currentAngel = i / ((CGFloat)self.itemButtons.count + 1);
        CGPoint farPoint = [self createEndPointWithRadius:self.bloomRadius + 5.0f andAngel:currentAngel];
        
        CAAnimationGroup *foldAnimation = [self foldAnimationFromPoint:itemButton.center withFarPoint:farPoint];
        
        [itemButton.layer addAnimation:foldAnimation forKey:@"foldAnimation"];
        itemButton.center = self.expandCenter;
        
    }
    
    [self bringSubviewToFront:self.pathCenterButton];
}

/**
 显示折叠动画
 */
- (void)showFoldAnimaton{
    
    AudioServicesPlaySystemSound(self.foldSound);
    [self resetItemLabel];
    [self resetItemBtnAnimation];
    [self hideBottomView];
    [self resetCenterBtn];
    [self resetFoldInfo];
    
}

#pragma mark - 按钮盛开动画
/**
 显示底部背景
 */
- (void)showBottomView{
     [self insertSubview:self.bottomView belowSubview:self.pathCenterButton];
    [UIView animateWithDuration:0.0618f * 3
                          delay:0.0f
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         _bottomView.alpha = 0.618f;
                     }
                     completion:nil];
}

/**
 旋转主按钮
 */
- (void)rotateCenterBtn{
    [UIView animateWithDuration:0.1575f
                     animations:^{
                         _pathCenterButton.transform = CGAffineTransformRotate(_pathCenterButton.transform,-0.75f * M_PI);
                         
                     }];
}

/**
 初始化子按钮信息
 */
- (void)setupItemBtnInfo{
    for (int i = 1; i <= self.itemButtons.count; i++) {
        PathItemButton *itemBtn = self.itemButtons[i - 1];
        itemBtn.delegate = self;
        itemBtn.tag = i - 1;
        itemBtn.transform = CGAffineTransformMakeTranslation(1, 1);
        itemBtn.alpha = 1.0f;
        itemBtn.center = self.expandCenter;
        [self insertSubview:itemBtn belowSubview:self.pathCenterButton];
    }
}

/**
 实现盛开组合动画
 */
- (CAAnimationGroup *)bloomAnimationWithEndPoint:(CGPoint)endPoint andFarPoint:(CGPoint)farPoint andNearPoint:(CGPoint)nearPoint{
    
    //旋转动画
    CAKeyframeAnimation *rotationAnimation = [CAKeyframeAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.values = @[@(0.0), @(- M_PI), @(- M_PI * 1.5), @(- M_PI * 2)];
    rotationAnimation.duration = 0.3f;
    rotationAnimation.keyTimes = @[@(0.0), @(0.3), @(0.6), @(1.0)];
    
    //平移动画
    CAKeyframeAnimation *movingAnimation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, self.expandCenter.x, self.expandCenter.y);
    
    CGPathAddLineToPoint(path, NULL, farPoint.x, farPoint.y);
    CGPathAddLineToPoint(path, NULL, nearPoint.x, nearPoint.y);
    CGPathAddLineToPoint(path, NULL, endPoint.x, endPoint.y);
    
    movingAnimation.path = path;
    movingAnimation.keyTimes = @[@(0.0), @(0.0), @(0.5), @(1.0)];
    movingAnimation.duration = 0.5f;
    CGPathRelease(path);
    
    //合并动画
    CAAnimationGroup *animations = [CAAnimationGroup animation];
    animations.animations = @[movingAnimation, rotationAnimation];
    animations.duration = 0.5f;
    animations.delegate = self;
    return animations;
}

/**
 初始化子按钮动画路径
 */
- (void)setupItemBtnAnimation{
    CGFloat gap = SCREEN_WIDTH/4;
    CGFloat itemX = gap/2, itemY = SCREEN_HEIGHT - 350;
    for (int i = 1; i <= self.itemButtons.count; i++) {
        PathItemButton *itemBtn = self.itemButtons[i - 1];
        CGFloat itemAngel = i / ((CGFloat)self.itemButtons.count + 1);
        //farPoint
        CGPoint farPoint = [self createEndPointWithRadius:self.bloomRadius + 10.0f andAngel:itemAngel];
        //nearPoint
        CGPoint nearPoint = [self createEndPointWithRadius:self.bloomRadius - 5.0f andAngel:itemAngel];
        //endPoint
        CGPoint endPoint = CGPointMake(itemX, itemY);
        itemX += gap;
        if (itemX > SCREEN_WIDTH) {
            itemX = gap/2;
            itemY += 100;
        }
        CAAnimationGroup *bloomAnimation = [self bloomAnimationWithEndPoint:endPoint
                                                                andFarPoint:farPoint
                                                               andNearPoint:nearPoint];
        [itemBtn.layer addAnimation:bloomAnimation forKey:@"bloomAnimation"];
        itemBtn.center = endPoint;
    }
}

/**
 初始化标签
 */
- (void)setupItemBtnLabel{
    NSArray *titleArray = @[@"学习",@"运动",@"电影",@"郊游",@"出行",@"桌游",@"聚会",@"美食",@"联谊",@"开黑",@"其它"];
    for (int i = 1; i <= self.itemButtons.count; i++) {
        PathItemButton *itemBtn = self.itemButtons[i - 1];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(itemBtn.center.x-30, itemBtn.center.y+30, 60, 30)];
        label.textColor = [UIColor whiteColor];
        label.font = [UIFont systemFontOfSize:13];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = titleArray[i-1];
        [self addSubview:label];
    }
}

/**
 显示盛开动画
 */
- (void)showBloomAnimaton{
    AudioServicesPlaySystemSound(self.bloomSound);
    self.expandCenter = self.center;
    self.frame = CGRectMake(0, 0, self.bloomSize.width, self.bloomSize.height);
    self.center = CGPointMake(self.bloomSize.width / 2, self.bloomSize.height / 2);
    
    [self showBottomView];//显示背景
    [self rotateCenterBtn];//旋转住按钮
    self.pathCenterButton.center = self.expandCenter;
    
    [self setupItemBtnInfo];//初始化子按钮信息
    [self setupItemBtnAnimation];//初始化子按钮动画路径
    [self setupItemBtnLabel];// 初始化子按钮标题
    _bloom = YES;
}

//增加子按钮
- (void)addPathItems:(NSArray *)pathItemButtons{
    [self.itemButtons addObjectsFromArray:pathItemButtons];
}

- (void)addPathItem:(id)pathItemButton{
    [self.itemButtons addObject:pathItemButton];
}

#pragma mark - getters and setters
//配置中心按钮图片
- (void)setCenterImage:(UIImage *)centerImage{
    if (!centerImage) {
        return ;
    }
    _centerImage = centerImage;
}

- (void)setCenterHighlightedImage:(UIImage *)highlightedImage{
    if (!highlightedImage) {
        return ;
    }
    _centerHighlightedImage = highlightedImage;
}

//配置扩展后的中心点
- (void)setExpandCenter:(CGPoint)centerButtonBloomCenter{
    if (_expandCenter.x == 0) {
        _expandCenter = centerButtonBloomCenter;
    }
    return ;
}

//扩展状态
- (BOOL)isBloom{
    
    return _bloom;
}
@end
