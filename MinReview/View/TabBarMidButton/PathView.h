//
//  PathView.h
//  MinReview
//
//  Created by zhengyumin on 17/5/23.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "PathItemButton.h"

@import UIKit;
@import QuartzCore;
@import AudioToolbox;

@protocol PathViewDelegate <NSObject>

- (void)itemButtonTappedAtIndex:(NSUInteger)index;

@end

@interface PathView : UIView

@property (weak, nonatomic) id<PathViewDelegate> delegate;

@property (strong, nonatomic) NSMutableArray *itemButtonImages;
@property (strong, nonatomic) NSMutableArray *itemButtonHighlightedImages;

@property (strong, nonatomic) UIImage *itemButtonBackgroundImage;
@property (strong, nonatomic) UIImage *itemButtonBackgroundHighlightedImage;

@property (assign, nonatomic) CGFloat bloomRadius;

@property (assign, nonatomic) CGPoint foldCenter;
@property (assign, nonatomic) CGPoint bloomCenter;

- (id)initWithCenterImage:(UIImage *)centerImage hilightedImage:(UIImage *)centerHighlightedImage;
- (void)addPathItems:(NSArray *)pathItemButtons;
- (void)addPathItem:(id)pathItemButton;
- (void)centerButtonTapped;

@end
