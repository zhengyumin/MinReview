//
//  PathCenterButton.h
//  MinReview
//
//  Created by zhengyumin on 17/5/23.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PathCenterButtonDelegate <NSObject>

- (void)centerButtonTapped;/**<TabBar中心按钮点击事件>*/

@end

@interface PathCenterButton : UIImageView

@property (weak, nonatomic) id<PathCenterButtonDelegate> delegate;

@end
