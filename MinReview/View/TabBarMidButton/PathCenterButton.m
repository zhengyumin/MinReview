//
//  PathCenterButton.m
//  MinReview
//
//  Created by zhengyumin on 17/5/23.
//  Copyright © 2017年 hikvision. All rights reserved.
//

#import "PathCenterButton.h"

@implementation PathCenterButton

/**
 初始化
 @param image 默认图片
 @param highlightedImage 高亮图片
 @return 动画主按钮
 */
- (id)initWithImage:(UIImage *)image highlightedImage:(UIImage *)highlightedImage{
    if (self = [super initWithImage:image highlightedImage:highlightedImage]) {
        self.userInteractionEnabled = YES;
        self.image = image;
        self.highlightedImage = highlightedImage;
    }
    return self;
}

#pragma mark - Event Response
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    self.highlighted = YES;
    if ([_delegate respondsToSelector:@selector(centerButtonTapped)]) {
        [_delegate centerButtonTapped];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    CGPoint currentLocation = [[touches anyObject]locationInView:self];
    if (!CGRectContainsPoint([self scaleRect:self.bounds] , currentLocation)) {
        self.highlighted = NO;
        return ;
    }
    self.highlighted = YES;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    self.highlighted = NO;
}

#pragma mark - Private Methods
//比例放大
- (CGRect)scaleRect:(CGRect)originRect{
    return CGRectMake(- originRect.size.width,
                      - originRect.size.height,
                      originRect.size.width * 3,
                      originRect.size.height * 3);
}

/**
 *  只要覆盖了这个方法,按钮就不存在高亮状态
 */
- (void)setHighlighted:(BOOL)highlighted{
    //    [super setHighlighted:highlighted];
}

@end
